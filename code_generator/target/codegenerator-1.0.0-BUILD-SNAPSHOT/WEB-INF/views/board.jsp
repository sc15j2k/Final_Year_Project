<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script id="code">
    function init() {
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
            initialContentAlignment: go.Spot.Center,
            allowDrop: true,  // must be true to accept drops from the Palette
            "LinkDrawn": showLinkLabel,  // this DiagramEvent listener is defined below
            "LinkRelinked": showLinkLabel,
            scrollsPageOnFocus: false,
            "undoManager.isEnabled": true  // enable undo & redo
        });
        // when the document is modified, add a "*" to the title and enable the "Save" button
        myDiagram.addDiagramListener("Modified", function(e) {
            var button = document.getElementById("SaveButton");
            if (button) button.disabled = !myDiagram.isModified;
            var idx = document.title.indexOf("*");
            if (myDiagram.isModified) {
                if (idx < 0) document.title += "*";
            } else {
                if (idx >= 0) document.title = document.title.substr(0, idx);
            }
        });
        // helper definitions for node templates
        function nodeStyle() {
            return [
                // The Node.location comes from the "loc" property of the node data,
                // converted by the Point.parse static method.
                // If the Node.location is changed, it updates the "loc" property of the node data,
                // converting back using the Point.stringify static method.
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                {
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center,
                //isShadowed: true,
                //shadowColor: "#888",
                // handle mouse enter/leave events to show/hide the ports
                mouseEnter: function (e, obj) { showPorts(obj.part, true); },
                mouseLeave: function (e, obj) { showPorts(obj.part, false); }
                }
            ];
        }
        // Define a function for creating a "port" that is normally transparent.
        // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
        // and where the port is positioned on the node, and the boolean "output" and "input" arguments
        // control whether the user can draw links from or to the port.
        function makePort_if(name, portName, leftside) {
            var port = $(go.Shape, "Rectangle",
                        {
                            fill: "gray", stroke: null,
                            desiredSize: new go.Size(8, 8),
                            portId: portName,  // declare this object to be a "port"
                            toMaxLinks: 1,  // don't allow more than one link into a port
                            cursor: "pointer"  // show a different cursor to indicate potential link point
                        });

            var lab = $(go.TextBlock, name,  // the name of the port
                        { font: "12pt sans-serif" });

            var panel = $(go.Panel, "Horizontal",
                            { margin: new go.Margin(2, 0) });

            // set up the port/panel based on which side of the node it will be on
            if (leftside) {
                port.fromSpot = go.Spot.Left;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 0, 0, 1);
                panel.alignment = go.Spot.Left;
                panel.add(port);
                panel.add(lab);
            } else {
                port.fromSpot = go.Spot.Right;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 1, 0, 0);
                panel.alignment = go.Spot.Right;
                panel.add(lab);
                panel.add(port);
            }
            return panel;
        }
        
        function makePort_while_for(name, portName) {
            var port = $(go.Shape, "Rectangle",
                        {
                            fill: "gray", stroke: null,
                            desiredSize: new go.Size(8, 8),
                            portId: portName,  // declare this object to be a "port"
                            toMaxLinks: 1,  // don't allow more than one link into a port
                            cursor: "pointer"  // show a different cursor to indicate potential link point
                        });

            var lab = $(go.TextBlock, name,  // the name of the port
                        { font: "12pt sans-serif" });

            var panel = $(go.Panel, "Horizontal",
                            { margin: new go.Margin(2, 0) });
            var panel2 = $(go.Panel, "Vertical",
                            { margin: new go.Margin(2, 0) });

            // set up the port/panel based on which side of the node it will be on
            if (portName == "B") {
                port.fromSpot = go.Spot.Bottom;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 0, 1, 0);
                panel.alignment = go.Spot.Bottom;
                panel.add(port);
                panel.add(lab);
            }else if (portName == "L") {
                port.toSpot = go.Spot.Left;
                port.toLinkable = true;
                lab.margin = new go.Margin(1, 0, 0, 1);
                panel.alignment = go.Spot.Left;
                panel.add(port);
                panel.add(lab);
            }else if (portName == "R") {
                port.fromSpot = go.Spot.Right;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 1, 0, 0);
                panel.alignment = go.Spot.Right;
                panel.add(lab);
                panel.add(port);
            }
            return panel;
        }

        function makePort(name, spot, output, input) {
        // the port is basically just a small circle that has a white stroke when it is made visible
            return $(go.Shape, "Circle",
                {
                    fill: "transparent",
                    stroke: null,  // this is changed to "white" in the showPorts function
                    desiredSize: new go.Size(8, 8),
                    alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                    portId: name,  // declare this object to be a "port"
                    fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                    fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                    cursor: "pointer"  // show a different cursor to indicate potential link point
                });
        }
        // define the Node templates for regular nodes
        var lightText = 'whitesmoke';
        myDiagram.nodeTemplateMap.add("",  // the default category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Rectangle",
                { fill: "#00A9C9", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                $(go.TextBlock,{
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 8,
                    maxSize: new go.Size(160, NaN),
                    wrap: go.TextBlock.WrapFit,
                    editable: true
                },new go.Binding("text").makeTwoWay())
                )
            ),
            // four named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort("B", go.Spot.Bottom, true, false)
        ));
        myDiagram.nodeTemplateMap.add("Start",
        $(go.Node, "Spot", nodeStyle(),
            $(go.Panel, "Auto",
            $(go.Shape, "Circle",
                { minSize: new go.Size(40, 40), fill: "#79C900", stroke: null }),
            $(go.TextBlock, "Start",
                { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
                new go.Binding("text"))
            ),
            // three named ports, one on each side except the top, all output only:
            makePort("L", go.Spot.Left, true, false),
            makePort("R", go.Spot.Right, true, false),
            makePort("B", go.Spot.Bottom, true, false)
        ));
        myDiagram.nodeTemplateMap.add("End",
        $(go.Node, "Spot", nodeStyle(),
            $(go.Panel, "Auto",
            $(go.Shape, "Circle",
                { minSize: new go.Size(40, 40), fill: "#DC3C00", stroke: null }),
            $(go.TextBlock, "End",
                { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
                new go.Binding("text"))
            ),
            // three named ports, one on each side except the bottom, all input only:
            makePort("T", go.Spot.Top, false, true),
            makePort("L", go.Spot.Left, false, true),
            makePort("R", go.Spot.Right, false, true)
        ));
 		myDiagram.nodeTemplateMap.add("If",  // the if category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a diamond Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Diamond",
                { fill: "#E88800", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                $(go.TextBlock,{
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 8,
                    maxSize: new go.Size(160, NaN),
                    wrap: go.TextBlock.WrapFit,
                    editable: true
                	},new go.Binding("text").makeTwoWay())
                )
           	),
            // three named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort_if("True","L",true),
            makePort_if("False","R",false)
			// makePort("L", go.Spot.Left, true, false),
	        // makePort("R", go.Spot.Right, true, false)
        ));
 		myDiagram.nodeTemplateMap.add("End if",
 		$(go.Node, "Spot", nodeStyle(),
 			$(go.Panel, "Auto",
 		    $(go.Shape, "Circle",
 		    	{ minSize: new go.Size(40, 40), fill: "#E88800", stroke: null }),
 		    $(go.TextBlock, "End if",
 		        { font: "bold 11pt Helvetica, Arial, sans-serif" },
 		        new go.Binding("text"))
 		    ),
 		    // three named ports, one on each side except the bottom, all input only:
 		    makePort("T", go.Spot.Top, false, true),
 		   	makePort("B", go.Spot.Bottom, true, false)
 		));
 		myDiagram.nodeTemplateMap.add("Print",  // the print category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a output Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Output",
                { fill: "#0BFFC8", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                $(go.TextBlock,{
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 8,
                    maxSize: new go.Size(160, NaN),
                    wrap: go.TextBlock.WrapFit,
                    editable: true
                	},new go.Binding("text").makeTwoWay())
                )
           	),
            // two named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort("B", go.Spot.Bottom, true, false)
        ));
 		myDiagram.nodeTemplateMap.add("Method",  // the method category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Rectangle",
                { fill: "#8653FF", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                $(go.TextBlock,{
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 8,
                    maxSize: new go.Size(160, NaN),
                    wrap: go.TextBlock.WrapFit,
                    editable: true
                	},new go.Binding("text").makeTwoWay())
                )
            ),
            // four named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
        	makePort("R", go.Spot.Right, true, false),
        	makePort("L", go.Spot.Left, true, false),
        	makePort("B", go.Spot.Bottom, false, true)
        ));
 		myDiagram.nodeTemplateMap.add("Return",  // the method category
 		$(go.Node, "Spot", nodeStyle(),
 			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
 		    $(go.Panel, "Auto",
 		    $(go.Shape, "Rectangle",
 		    	{ fill: "#8653FF", stroke: null },
 		    	new go.Binding("figure", "figure")),
 		    	$(go.Panel, "Vertical",
 		        $(go.TextBlock, 
 		        	new go.Binding("text", "category"),
 		        	new go.Binding("background", "fill"),{ 
 		            font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 2,
 		            isMultiline: false }),
 		        $(go.TextBlock,{
 		        	font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 8,
 		            maxSize: new go.Size(160, NaN),
 		            wrap: go.TextBlock.WrapFit,
 		            editable: true
 		            },new go.Binding("text").makeTwoWay())
 		        )
 		    ),
 		    // two named ports, one on each side:
 		    makePort("T", go.Spot.Top, false, true),
 		    makePort("B", go.Spot.Bottom, true, false)
 		));
 		myDiagram.nodeTemplateMap.add("For",  // the if category
 		$(go.Node, "Spot", nodeStyle(),
 			// the main object is a Panel that surrounds a TextBlock with a diamond Shape
 		    $(go.Panel, "Auto",
 		    $(go.Shape, "Diamond",
 		    	{ fill: "#FF22FC", stroke: null },
 		        new go.Binding("figure", "figure")),
 		        $(go.Panel, "Vertical",
 		        $(go.TextBlock, 
 		        	new go.Binding("text", "category"),
 		            new go.Binding("background", "fill"),{ 
 		            font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 2,
 		            isMultiline: false }),
 		        $(go.TextBlock,{
 		        	font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 8,
 		            maxSize: new go.Size(160, NaN),
 		            wrap: go.TextBlock.WrapFit,
 		            editable: true
 		        	},new go.Binding("text").makeTwoWay())
 		        )
 		    ),
 		    // three named ports, one on each side:
            // makePort("L", go.Spot.Left, false, true),
 			// makePort("R", go.Spot.Right, true, false),
 		    makePort("T", go.Spot.Top, false, true),
            makePort_while_for("Continue","L"),
            makePort_while_for("Escape","R"),
            makePort_while_for("True","B")
			// makePort("B", go.Spot.Bottom, true, false)
 		));
 		myDiagram.nodeTemplateMap.add("While",  // the if category
 		$(go.Node, "Spot", nodeStyle(),
 			// the main object is a Panel that surrounds a TextBlock with a diamond Shape
 		    $(go.Panel, "Auto",
 		    $(go.Shape, "Diamond",
 		    	{ fill: "#FF2476", stroke: null },
 		        new go.Binding("figure", "figure")),
 		        $(go.Panel, "Vertical",
 		        $(go.TextBlock, 
 		        	new go.Binding("text", "category"),
 		            new go.Binding("background", "fill"),{ 
 		            font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 2,
 		            isMultiline: false }),
 		        $(go.TextBlock,{
 		        	font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 8,
 		            maxSize: new go.Size(160, NaN),
 		            wrap: go.TextBlock.WrapFit,
 		            editable: true
 		            },new go.Binding("text").makeTwoWay())
 		        )
 		    ),
 		    // three named ports, one on each side:
 		    makePort("T", go.Spot.Top, false, true),
            makePort_while_for("Continue","L"),
            makePort_while_for("Escape","R"),
            makePort_while_for("True","B")
 			// makePort("L", go.Spot.Left, false, true),
 			// makePort("R", go.Spot.Right, true, false),
 			// makePort("B", go.Spot.Bottom, true, false)
 		));
        

    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        {
          routing: go.Link.AvoidsNodes,
          curve: go.Link.JumpOver,
          corner: 5, toShortLength: 4,
          relinkableFrom: true,
          relinkableTo: true,
          reshapable: true,
          resegmentable: true,
          // mouse-overs subtly highlight links:
          mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
          mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
        },
        new go.Binding("points").makeTwoWay(),
        $(go.Shape,  // the highlight shape, normally transparent
          { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
        $(go.Shape,  // the link path shape
          { isPanelMain: true, stroke: "gray", strokeWidth: 2 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null, fill: "gray"}),
        $(go.Panel, "Auto",  // the link label, normally not visible
          { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5},
          new go.Binding("visible", "visible").makeTwoWay(),
          $(go.Shape, "RoundedRectangle",  // the label shape
            { fill: "#F8F8F8", stroke: null }),
          $(go.TextBlock, "True",  // the label
            {
              textAlign: "center",
              font: "10pt helvetica, arial, sans-serif",
              stroke: "#333333",
              editable: true
            },
            new go.Binding("text").makeTwoWay())
        )
      );

    // Make link labels visible if coming out of a "conditional" node.
    // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
    function showLinkLabel(e) {
      var label = e.subject.findObject("LABEL");
      if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
    }

        // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
        myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
        myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;
        load();  // load an initial diagram from some JSON text
    
        // basic
        var myPalette = $(go.Palette, "myPaletteDiv");
        myPalette.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette.model.nodeDataArray =[
            { category: "Variable", text: "Variable: [type] [name] [value]" },
            { category: "Equal", text: "Equal: [A] = [B]", figure: "SquareArrow"},
            { category: "Start", text: "Start" },
            { category: "End", text: "End" }
        ];
     	// array
       	var myPalette2 = $(go.Palette, "myPaletteDiv2");
        myPalette2.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette2.model.nodeDataArray =[
            { category: "Array", text: "Array: [type] [name]", figure: "rectangle" },
            { category: "Append", text: "Append [target array] [value or variable]", figure: "SquareArrow"},
            { category: "Insert", text: "Insert [target array] [location] [value or variable]", figure: "SquareArrow"},
            { category: "Remove", text: "Remove [target array] [location]", figure: "SquareArrow"}
        ];
        // loop
        var myPalette3 = $(go.Palette, "myPaletteDiv3");
        myPalette3.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette3.model.nodeDataArray =[
            { category:"If", text: "If [condition]", figure: "Diamond" },
            { category: "End if",  figure: "Circle" },
            { category:"For", text: "For [minimum value] to [maximum value] step [value]", figure: "Diamond" },
            { category:"While", text: "While [condition]", figure: "Diamond" }
        ];
    
        // function
        var myPalette4 = $(go.Palette, "myPaletteDiv4");
        myPalette4.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette4.model.nodeDataArray =[
            { category:"Method", text: "Func [return type] [method name] [parameter]"},
            { category:"Return", text: "Return [variable name or value]"},
            { category:"Print", text: "Print: [content]"}
        ];
    
        document.getElementById("defaultOpen").click();
    } // end init
    // Make all ports on a node visible when the mouse is over the node
    function showPorts(node, show) {
        var diagram = node.diagram;
        if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
        node.ports.each(function(port) {
        port.stroke = (show ? "white" : null);
      });
    }
    // Show the diagram's model in JSON format that the user may edit
    function save() {
        var xxx = myDiagram.model.toJson();
        var sss = JSON.parse(myDiagram.model.toJson());
        document.getElementById("mySavedModel").value = myDiagram.model.toJson();
        // alert(sss.nodeDataArray[0].category);
        var if_count=0;
        var endif_count=0;
        for(var i = 0; i<sss.nodeDataArray.length; i++){
            var current_category = sss.nodeDataArray[i].category;
            if(current_category == "If"){
                if_count++;
            }
            if(current_category == "End if"){
                endif_count++;
            }
        }
        if(if_count!=endif_count){
            alert("WARNING :: You must only use 'End if' element after finish making if statement. See how to use it in the documents or tutorial.")
        }else{
        	var aaaaa ="";
        	for(var j = 0; j<sss.linkDataArray.length; j++){
        		var current_category2 = sss.linkDataArray[j];
        		aaaaa+=current_category2+"\n";
        	}
            alert(aaaaa);
        }
        // alert(sss.nodeDataArray.length);
        myDiagram.isModified = false;
    }
    function load() {
        myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    }
  
    function send(){
	  var theForm, newInput;
	  var data = myDiagram.model.toJson();
	  
	  theForm = document.createElement('form');
	  theForm.action = "/create";
	  theForm.method = "post";
	  
	  newInput = document.createElement('input');
	  newInput.type = 'text';
	  newInput.id = 'flowchart_data';
	  newInput.name = 'flowchart_data';
	  newInput.value = data;
	  
	  theForm.appendChild(newInput);
	  
	  document.getElementById('hidden_form_container').appendChild(theForm);
	  theForm.submit();
    }
  
    function changePalette(evt,cityName) {
	    if(cityName == "myPaletteDiv"){
			document.getElementById("myPaletteDiv").style.display="";
		    document.getElementById("myPaletteDiv2").style.display="none";
		    document.getElementById("myPaletteDiv3").style.display="none";
		    document.getElementById("myPaletteDiv4").style.display="none";
	    } else if(cityName == "myPaletteDiv2"){
		    document.getElementById("myPaletteDiv").style.display="none";
		    document.getElementById("myPaletteDiv2").style.display="";
		    document.getElementById("myPaletteDiv3").style.display="none";
		    document.getElementById("myPaletteDiv4").style.display="none";
	    }else if(cityName == "myPaletteDiv3"){
		    document.getElementById("myPaletteDiv").style.display="none";
		    document.getElementById("myPaletteDiv2").style.display="none";
		    document.getElementById("myPaletteDiv3").style.display="";
		    document.getElementById("myPaletteDiv4").style.display="none";
	    }else if(cityName == "myPaletteDiv4"){
			document.getElementById("myPaletteDiv").style.display="none";
			document.getElementById("myPaletteDiv2").style.display="none";
			document.getElementById("myPaletteDiv3").style.display="none";
			document.getElementById("myPaletteDiv4").style.display="";
		}
    }
  
</script>
</head>
<body onload="init()">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">Logo</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="/">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
                
            </div>
        </div>
    </nav>
    <h3 style="padding-left: 15px;">Flowchart Board</h3>
    <div class="container-fluid text-center" style="padding-top:3px">
        <div id="sample">
            <div style="width: 100%; padding-left: 0px; padding-right: 0px; display: flex; justify-content: space-between">
                <div class="tab">
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv')" id="defaultOpen">Basic</button>
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv2')">Array</button>
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv3')">Loops</button>
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv4')">Function</button>
                </div>
                <div id="myPaletteDiv" style="width: 450px; height: 550px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myPaletteDiv2" style="width: 450px; height: 550px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myPaletteDiv3" style="width: 450px; height: 550px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myPaletteDiv4" style="width: 450px; height: 550px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myDiagramDiv" style="flex-grow: 1; height: 550px; border: solid 1px black"></div>
                <div><p>temp space</p></div>
            </div>
            <p>
            Mouse-over a Node to view its ports.
            Drag from these ports to create new Links.
            Selecting Links allows you to re-shape and re-link them.
            Selecting a Node and then clicking its TextBlock will allow
            you to edit text (except on the Start and End Nodes).
            </p>
            <button id="SaveButton" onclick="send()">Send</button>
            <button id="SaveButton" onclick="save()">Save</button>
            <button onclick="load()">Load</button>
            <div id="hidden_form_container" style="display:none;"></div>
            <textarea id="mySavedModel" style="display:;width:100%;height:300px">
                { "class": "go.GraphLinksModel",
                "linkFromPortIdProperty": "fromPort",
                "linkToPortIdProperty": "toPort",
                "nodeDataArray": [],
                "linkDataArray": []}
            </textarea>
        </div>
    </div>
    <footer class="container-fluid text-center">
        <p>Footer Text</p>
    </footer>
</body>
</html>
<!--  -->