package com.janghoon.codegenerator.tutorial;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TutorialController {

	@RequestMapping(value = "/tutorial")
	public ModelAndView tutorial(ModelAndView mv) {
		// open the tutorial page
		String page = "tutorial";
		mv.setViewName(page);
		return mv;
	}
	/*
	 * 
	 * Sub pages of the tutorial section
	 * 
	 */
	// need to fix
	@RequestMapping(value = "/tutorial/basic")
	public String tutorial_basic() {
		String page = "basic";
		// move to tutorial/basic page
		return page;
	}
	
	@RequestMapping(value = "/tutorial/operation")
	public String tutorial_operation() {
		String page = "operation";
		// move to tutorial/operation page
		return page;
	}
	
	@RequestMapping(value = "/tutorial/array")
	public String tutorial_array(Model model) {
		String page = "array";
		// move to tutorial/array page
		return page;
	}

	@RequestMapping(value = "/tutorial/statement")
	public String tutorial_statement(Model model) {
		String page = "statement";
		// move to tutorial/loop page
		return page;
	}

	@RequestMapping(value = "/tutorial/function")
	public String tutorial_func(Model model) {
		String page = "function";
		// move to tutorial/function page
		return page;
	}

	@RequestMapping(value = "/tutorial/example")
	public String example(Model model) {
		String page = "example";
		// move to tutorial/example page
		return page;
	}
}
