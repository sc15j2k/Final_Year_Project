package com.janghoon.codegenerator.translate;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Repository;

@Repository
public class ConvertImpl implements ConvertDAO {
	
	@Override
	public String generatePseudocode(JSONObject jsonObject) {
		// initialise the variable which save the pseudo-code
		String pseudocode = "";
      	// initialise hash maps which save the location, category and content of the shapes, the list of key and tab sizes
		HashMap<Integer,String> textContent = new HashMap<Integer,String>();
        HashMap<Integer,String> categoryContent = new HashMap<Integer,String>();
        HashMap<String,Integer> keyList = new HashMap<String,Integer>();
        HashMap<String,Integer> tabSize = new HashMap<>();
        // initialise array lists which save the order of the flowchart and the list of methods that user generated before
        ArrayList<String> textOrder = new ArrayList<>();
        ArrayList<String> methodList = new ArrayList<>();
 
        // call the object which utilise to generate the pseudo-code
        ConvertPesudocodeFunctionList server = new ConvertPesudocodeFunctionList();
    	
        // get the node data from the flowchart
		JSONArray nodeDataArray = (JSONArray) jsonObject.get("nodeDataArray");
        // get the link data from the flowchart
		JSONArray linkDataArray = (JSONArray) jsonObject.get("linkDataArray");
        
		// save the location, category and content of the shapes
        server.storeNodeData(textContent, categoryContent, keyList, nodeDataArray);
        // get the location of starting point in the main method part
        int startFrom = keyList.get("Start");
        
        
        
        
        // set the order of the node from flowchart which will use to generate the pseudo-code
        textOrder = server.buildCodeStructure(textContent, categoryContent, methodList, linkDataArray, startFrom, server);
        
        // generate the pseudo-code from the order of the node, method list and the tab size list
        pseudocode = server.makePseudocode(methodList, textOrder, tabSize);
        
        // initialise the object because it does not use again
        server = null;
        
        System.out.println(pseudocode);
     
        return pseudocode;
	}
	
	@Override
	public String generateSourceCode(String type, String pseudocode) {
		// initialise the variable which saves the source code
		String sourceCode = "";
		// call the object which utilise to generate the source code
		ConvertSourceCodeFunctionList server = new ConvertSourceCodeFunctionList();
		// generate the source code from the pseudo-code
		sourceCode = server.makeSourceCode(type, pseudocode);
		// initialise the object because it does not use again
		server = null;
		
		return sourceCode;
	}
}
