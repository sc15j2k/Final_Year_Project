package com.janghoon.codegenerator.translate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ConvertPesudocodeFunctionList {

	// get the next destination from the right path of the departure node
	public int returnMultipleTo_R(JSONArray linkDataArray, int fromValue) {
        long from = 0;
        long to = 0;
        String fromPort = "";
        for(int i=0; i<linkDataArray.size(); i++){
            JSONObject result = (JSONObject) linkDataArray.get(i);
            from = (long) result.get("from");
            fromPort = (String) result.get("fromPort");
            // if current node is departure node
            if(from == (long) fromValue){
            	// if path is right direction
                if(fromPort.contains("R")){
                    to = (long) result.get("to");
                    break;
                }
            }
        }
        // return next destination node location
        return (int) to;
    }
	// get the next destination from the left path of the departure node
    public int returnMultipleTo_L(JSONArray linkDataArray, int fromValue) {
        long from = 0;
        long to = 0;
        String fromPort = "";
        for(int i=0; i<linkDataArray.size(); i++){
            JSONObject result = (JSONObject) linkDataArray.get(i);
            from = (long) result.get("from");
            fromPort = (String) result.get("fromPort");
            // if current node is departure node
            if(from == (long) fromValue){
            	// if path is left direction
                if(fromPort.contains("L")){
                    to = (long) result.get("to");
                    break;
                }
            }
        }
        // return next destination node location
        return (int) to;
    }
    // get the next destination from the normal (bottom) path of the departure node
    public int returnNormalTo(JSONArray linkDataArray, int fromValue) {
    	long from;
    	long to = 0;
    	String fromPort = "";
    	for(int i=0; i<linkDataArray.size(); i++) {
    		JSONObject result = (JSONObject) linkDataArray.get(i);
    		from = (long) result.get("from");
    		fromPort = (String) result.get("fromPort");
    		// if current node is departure node
    		if(from == (long) fromValue){
    			// if path is normal (bottom) direction
    			if(fromPort.contains("B")){
                    to = (long) result.get("to");
                    break;
                }
            }
    	}
    	// return next destination node location
    	return (int) to;
    }
    
    // get the next node value from the current node
    public int getToValue(JSONArray linkDataArray, int fromValue, String path) {
    	int to = 0;
    	switch(path) {
    		case "Left":
    			to = returnMultipleTo_L(linkDataArray,fromValue);
    			break;
    		case "Right":
    			to = returnMultipleTo_R(linkDataArray,fromValue);
    			break;
    		default:
    			to = returnNormalTo(linkDataArray,fromValue);
    			break;
    	}
    	return to;
    }
    // generate the pseudo-code
    public String makePseudocode(ArrayList<String> methodList, ArrayList<String> textOrder, HashMap<String,Integer> tabSize) {
    	// initialise the variables
    	// main method part in pseudo-code
    	String rawServerCode = "";
    	// the server-code logic
    	String logicOrder = "";
    	// overall pseudo-code
    	String pseudocode="";
    	// main() part code
    	String mainCode="";
    	// store the information of the current node
    	String currentLine;
    	// store the information of the previous node
        String prevLine = "";   
        // store the tab size
        int tab = 0;
        
        // Arrange unnecessary code lines
        for(int i = 0;i<textOrder.size();i++) {
        	currentLine = textOrder.get(i);
        	// When the else statement does not have any lines, remove else statement
        	if(currentLine.contains("}")) {
        		if(prevLine.contains("else")) {
        			textOrder.remove(i-1);
        		}
        	}
        	logicOrder += currentLine+"\n";
        	prevLine = currentLine;
        }
        
        // save the methods which made from user (in flowchart)
        HashMap<Integer,String> methodContent = new HashMap<>();
        // save the name of method
        HashMap<Integer,String> prevFuncCollection = new HashMap<>();
        // save the sequence of the code
        int index = 0;
        int index2 = 0;
        // it stores the name of the method 
        String prevFunc= "";
        // list of the main part server-code lines and raw content of the method part
        String[] logicFragments = logicOrder.split("\\n");
        // it stores the method content part
        String methodPart = "";
        
        // Save the methods on the method part and save codes from main function on the pseudocode
        for(String line : logicFragments) {
        	// if the line is inside of that method
        	if(prevFunc!=""){
        		// if the line includes the name of the method, add the method content into the hash map and add the name of the method into the hash map
        		if(methodList.contains(line)) {
        			methodContent.put(index,methodContent.get(index)+"\t"+line.replace("Func", "Method")+"\n");
        			index = methodList.indexOf(line);
        			index2+=1;
        			methodContent.put(index, line+"\n");
        			prevFuncCollection.put(index2, line);
        			prevFunc = prevFuncCollection.get(index);
        		}else { // if the line is inside of the current method,
        			// if the method part is finished, stop saving the current method content and remove the name of current method into the hash map
        			if(line.contains("};")) {
        				methodContent.put(index, methodContent.get(index)+"}");
        				prevFuncCollection.remove(index2);
        				if(prevFuncCollection.size()>0) {
        					index -=1 ;
        					index2 -=1 ;
        					prevFunc = prevFuncCollection.get(index2);
        				}else {
        					index = 0;
        					prevFunc = "";
        				}
        			}else { // save the line into the hash map
        				methodContent.put(index,methodContent.get(index)+"\t"+line+"\n");
        			}
        		}
        	}else { // if the line is the name of the method, save it into the hash map
        		if(methodList.contains(line)) {
        			index = methodList.indexOf(line);
        			methodContent.put(index, line+'\n');
        			prevFuncCollection.put(index2, line);
        			prevFunc = prevFuncCollection.get(index2);
        			mainCode+=line.replace("Func", "Method")+"\n";
        		}else { // if the line is not part of the method, save pseudo-code into the main part
        			mainCode+=line+"\n";
        		}
        	}
        }
        
    	// add method parts before the main part
        for(int i = 0; i<methodContent.size();i++) {
        	methodPart+=methodContent.get(i)+"\n";
        }
        
        rawServerCode = methodPart+mainCode;
        
        String[] serverCodeFragments = rawServerCode.split("\n");
        
        
        // reset the previous line variable to avoid collision between previous and next for loops
        prevLine = "";
        
        
        // make a pseudo-code
        for(String line : serverCodeFragments) {
        	currentLine = line;
        	if(prevLine.contains("If") || prevLine.contains("While") || prevLine.contains("For")) {
        		tab += 1;
        	} else if(prevLine.contains("else")) {
        		tab += 1;
        	} else if(currentLine.contains("else")) {
        		tab -= 1;
        	} else if(currentLine.contains("}")) {
        		tab -= 1;
        	} 
        	if(tab<0) {
        		tab=0;
        	}
        	tabSize.put(currentLine, tab);
        	if (tabSize.get(currentLine) == 0) {
        		pseudocode += currentLine+"\n";           		
        	}else {
        		String temp = "";
        		for(int i=0; i<tabSize.get(currentLine);i++) {
        			temp += "\t";
        		}
        		temp += currentLine;
        		pseudocode += temp+"\n";
        	}
        	prevLine = currentLine;
        }        
    	return pseudocode;
    }
    
    // save the content, category and order of the node into each hash map
    public void storeNodeData(HashMap<Integer,String> textContent, HashMap<Integer,String> categoryContent, HashMap<String,Integer> keyList, JSONArray nodeDataArray) {
        long keyValue = 0;
        String textValue = "";
        String categoryValue = "";
        String first="";
        String second="";
        String third="";
        for(int i=0; i<nodeDataArray.size(); i++){
        	String totalTextVal="";
            JSONObject result = (JSONObject) nodeDataArray.get(i);
            keyValue = (long) result.get("key");
            textValue = (String) result.get("text");
            categoryValue = (String) result.get("category");
            first = (String) result.get("first");
            second = (String) result.get("second");
            third = (String) result.get("third");
            switch(categoryValue) {
            case "Start":
            	totalTextVal=textValue;
            	break;
            case "End":
            	totalTextVal=textValue;
            	break;
            case "Variable":
            	totalTextVal=categoryValue+": ["+textValue+"] ["+first+"] ["+second+"]";
            	break;
            case "Equal":
            	totalTextVal=textValue+": ["+first+"] = ["+second+"]";
            	break;
            case "Array":
            	totalTextVal=textValue+": ["+first+"] ["+second+"]";
            	break;
            case "Append":
            	totalTextVal=textValue+" ["+first+"] ["+second+"]";
            	break;
            case "Insert":
            	totalTextVal=textValue+" ["+first+"] ["+second+"] ["+third+"]";
            	break;
            case "Remove":
            	totalTextVal=textValue+" ["+first+"] ["+second+"]";
            	break;
            case "If":
            	totalTextVal=textValue+" ["+first+"]";
            	break;
            case "End if":
            	totalTextVal=textValue;
            	break;
            case "For":
            	totalTextVal=textValue+" ["+first+"] to ["+second+"] step ["+third+"]";
            	break;
            case "While":
            	totalTextVal=textValue+" ["+first+"]";
            	break;
            case "Method":
            	totalTextVal=textValue+" ["+first+"] ["+second+"] ["+third+"]";
            	break;
            case "Return":
            	totalTextVal=textValue+" ["+first+"]";
            	break;
            case "Print":
            	totalTextVal=textValue+": ["+first+"]";
            	break;
            default:
            	totalTextVal="Equal: ["+first+"] = ["+second+textValue+third+"]";
            	break;
            }
            textContent.put((int) keyValue, totalTextVal);            
            categoryContent.put((int) keyValue, categoryValue);
            keyList.put(categoryValue, (int) keyValue);
        }
    }
    
    // sort the order of the flowchart structure
    public ArrayList<String> buildCodeStructure(HashMap<Integer,String> textContent, HashMap<Integer,String> categoryContent, ArrayList<String> methodList, JSONArray linkDataArray, int startValue, ConvertPesudocodeFunctionList server) {
        int from = 0;
        int to = 0;
        int rightTo = 0;
        boolean progress = true;
        ArrayList<String> textOrder = new ArrayList<>();
        HashMap<Integer,Integer> endIfStatementCount = new HashMap<>();
        LinkedHashMap<Integer,Integer> storeIfStatement = new LinkedHashMap<>();
        ArrayList<Integer> statementIfToQueue = new ArrayList<>();
        ArrayList<Integer> statementWhileForMethodQueue = new ArrayList<>();
        
        for(int i=0; i<linkDataArray.size(); i++) {
    		JSONObject result = (JSONObject) linkDataArray.get(i);
    		long fromValue = (long) result.get("from");
    		if(categoryContent.get((int) fromValue).equals("End if")) {
    			endIfStatementCount.put((int) fromValue, 0);
    		}
    	}
        
        to = startValue;
        // while loop, it will escape when there are no more flowchart nodes 
        while(progress) {
        	from = to;
        	String category = categoryContent.get(from);
        	// if the category of the node is 'IF' statement
        	if(category.contains("If")) {
        		rightTo = server.getToValue(linkDataArray,from,"Right");
        		statementIfToQueue.add(rightTo);
        		storeIfStatement.put(rightTo, from);
        		to = server.getToValue(linkDataArray,from,"Left");
        		textOrder.add(textContent.get(from)+"{");
        	}
        	// if the category of the node is 'else' statement
        	else if(category.contains("End if")) {
        		endIfStatementCount.put(from,endIfStatementCount.get(from)+1);
        		if(endIfStatementCount.get(from) < 2) {
        			int last_index = statementIfToQueue.size()-1;
        			to = statementIfToQueue.get(last_index);
        			int statement_info = storeIfStatement.get(to);
        			statementIfToQueue.remove(last_index);
        			if(categoryContent.get(statement_info).contains("If")) {
        				textOrder.add("}else{");
        			}
        		}else {
        			to = server.getToValue(linkDataArray,from,"Normal");
        			textOrder.add("}");
        		}
        	}
        	// if the flowchart is finished, escape this loop
        	else if(category.contains("End")) {
        		textOrder.add(textContent.get(from));
        		progress = false;
        	}
        	// if the category of the node is 'While' or 'For' statement
        	else if(category.contains("While") || category.contains("For")) {
        		// if it is first time
        		if(statementWhileForMethodQueue.contains(from)) {
        			rightTo = server.getToValue(linkDataArray,from,"Right");
        			to = rightTo;
        			textOrder.add("}");
        		}
        		// if it reads second time
        		else {
        			to = server.getToValue(linkDataArray,from,"Normal");
        			textOrder.add(textContent.get(from)+"{");
        			statementWhileForMethodQueue.add(from);
        		}
        	}
        	// if the category of the node is 'Method'
        	else if(category.contains("Method")){
        		// First time
        		if(statementWhileForMethodQueue.contains(from)) {
        			rightTo = server.getToValue(linkDataArray,from,"Left");
        			to = rightTo;
        			textOrder.add("};");
        		}
        		// Second time
        		else {
        			to = server.getToValue(linkDataArray,from,"Right");
        			textOrder.add(textContent.get(from));
        			statementWhileForMethodQueue.add(from);
        			methodList.add(textContent.get(from));
        		}
        	}
        	// if the category of the node belongs to the rest
        	else {
        		to = server.getToValue(linkDataArray,from,"Normal");
        		textOrder.add(textContent.get(from));
        	}
        }
		return textOrder;
    }
}
