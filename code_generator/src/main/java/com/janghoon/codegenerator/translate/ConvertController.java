package com.janghoon.codegenerator.translate;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ConvertController {
	// save JSON value and source code lines and share both two methods
	String sourceCode;
	String json_data;
	String programmingLanguage;
	// call interface object
	@Autowired
	ConvertDAO convertDAO;
	
	// the part of creating the pseudo-code and source code
	@RequestMapping(value = "/create")
	public String Create(@RequestParam String language, @RequestParam String flowchart_data) throws Exception{
		// save the JSON data into the variable
		json_data = flowchart_data;
		// initialise three variables to save the JSON object
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(json_data);
		JSONObject jsonObject = (JSONObject) obj;
		// generate the server-friendly pseudo-code 
		String pseudocode = convertDAO.generatePseudocode(jsonObject);
		// generate the actual source code from the pseudo-code
		programmingLanguage = language;
		sourceCode = convertDAO.generateSourceCode(programmingLanguage, pseudocode);
		// after finishing to generate source code, move to result page
		String page = "redirect:/result";
		return page;
	}
	
	// the part of showing the source code and the flowchart
	@RequestMapping(value = "/result")
	public String board(Model model) {
		// split the source code by each line because the html does not read <,> by plain text. So, I need to convert those characters
		String[] sourceCodeLines = sourceCode.split("\\n");
		String sourceCodeHTML="";
		for(String line : sourceCodeLines) {
			// if the code line has < or > character, convert those part
			if(line.contains("<")&&line.contains(">")) {
				String temp=line.replace("<", "&lt;").replace(">", "&gt;");
				sourceCodeHTML+=temp+"\n";
			}else if(line.contains("<")) {
				String temp=line.replace("<", "&lt;");
				sourceCodeHTML+=temp+"\n";
			}else {
				sourceCodeHTML+=line+"\n";				
			}
		}
		// send the flowchart data and source code to the result page
		String page = "result";
		System.out.println(sourceCodeHTML);
		model.addAttribute("JSON",json_data);
		model.addAttribute("SOURCECODE",sourceCodeHTML);
		model.addAttribute("language","\""+programmingLanguage+"\"");
		// move to result page
		return page;
	}
}
