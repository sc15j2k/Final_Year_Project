package com.janghoon.codegenerator.translate;

import org.json.simple.JSONObject;

// declare the necessary methods to implement the functionality of generating pseudo-code and source code
public interface ConvertDAO {
	public String generatePseudocode(JSONObject jsonObject);
	public String generateSourceCode(String type, String pseudocode);
}
