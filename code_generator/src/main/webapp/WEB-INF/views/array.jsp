<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>
<div id="body">
	<nav class="navbar navbar-inverse navbar-custom">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
      			</button>
      			<a class="navbar-brand" href="/">Code Generator</a>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
        			<li><a href="/">Home</a></li>
        			<li><a href="#">About</a></li>
        			<li><a href="#">Contact</a></li>
      			</ul>
    		</div>
  		</div>
	</nav>

	<div class="container-fluid text-center" style="padding-bottom:100px;">
	  	<div class="row content">
	    	<div class="col-sm-2 sidenav">
	      		<h4>Code Generator Tutorial</h4>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial">Tutorial Home</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/basic">Basic part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/operation">Operation part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px; background-color: #4CAF50;"><a href="/tutorial/array" class="active">Array part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/statement">Statement part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/function">Function part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/example">Example</a></p>
	    	</div>
	    	<div class="col-sm-8 text-left">
	      		<h1>Array part Tutorial</h1>
	    		<div class="cg-panel cg-info intro">
	    			<p>Array is one of the data structure in programming language.</p>
	    			<p>This tutorial helps you about handling array and some array-related methods in flowchart.</p>
	    		</div>
				<h3><a name="contents"></a>Contents</h3>
				<dl name="contents" class="toc-indent">
					<dt><span><a href="#s-1">1</a>. Array data type</span></dt>
					<dt><span><a href="#s-2">2</a>. Append method</span></dt>
					<dt><span><a href="#s-3">3</a>. Insert method</span></dt>
					<dt><span><a href="#s-4">4</a>. Remove method</span></dt>
				</dl>
				<br>
				<span><h2><a id="s-1" href="#contents">1.</a> Array data type</h2></span>
				<br>
				<p>An array is a data structure consisting of a collection of elements (values or variables).</p>
				<p>In this shape, you can edit the type and name of array inside of the [ ] brackets : <img src="/resources/images/array/unit/array_unit_raw.png"/></p>
				<p>There is a list of array types supported by each language.</p>
				<ul>
					<li style="font-size:16px;">Python: Character, Integer, Double and String</li>
					<li style="font-size:16px;">JAVA: Character, Integer, Double and String</li>
					<li style="font-size:16px;">C: Character, Integer and Double</li>
				</ul>
				<br><br>
				<h2>How to use each array type</h2>
				<p><b>Character:</b> type 'char' in the type section and type the name of the array in the name section</p>
				<p>Example:</p>
				<img src="/resources/images/array/unit/array_unit_char.png"/>
				<br><br>
				<p><b>Integer:</b> type 'int' in the type section and type the name of the array in the name section</p>
				<p>Example:</p>
				<img src="/resources/images/array/unit/array_unit_int.png"/>
				<br><br>
				<p><b>Double:</b> type 'double' in the type section and type the name of the array in the name section</p>
				<p>Example:</p>
				<img src="/resources/images/array/unit/array_unit_double.png"/>
				<br><br>
				<p><b>String:</b> type 'String' in the type section and type the name of the array in the name section</p>
				<p>Example:</p>
				<img src="/resources/images/array/unit/array_unit_string.png"/>
				<hr>
				<span><h2><a id="s-2" href="#contents">2.</a> Append method</h2></span>
				<br>
				<p>The append method item in flowchart adds a single element to the end of the array. Common error: does not return the new list, just modifies the original.</p>
				<p>In this shape, you can put the value or variable name and target name of array inside of the [ ] brackets : <img src="/resources/images/array/append/array_append_raw.png"/></p>
				<p>There are some examples for using append method by some types.<p>
				<p><b>Integer:</b></p>
				<img src="/resources/images/array/append/array_append_int.png"/>
				<p><b>Double:</b></p>
				<img src="/resources/images/array/append/array_append_double.png"/>
				<p><b>String / Character:</b></p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>You must use double quotation marks when you append the string value. Or use single quotation marks in character value.</p>
					</div>
				</div>
				<p>String : <img src="/resources/images/array/append/array_append_string.png"/></p>
				<p>Character :<img src="/resources/images/array/append/array_append_char.png"/></p>
				<hr>
				<span><h2><a id="s-3" href="#contents">3.</a> Insert method</h2></span>
				<br>
				<p>The insert method item in flowchart inserts the element at the given index (location), shifting elements to the right.</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>To set the location value, it starts from '0' not at '1'. So, if you want to put a value into 1st location, you should insert '0' into 'location' part.</p>
						<p>Therefore, the location value starts from 0. (like as 0,1,2,3,4, ... )</p>
					</div>
				</div>
				<p>In this shape, you can put the location value, value or variable name and target name of array inside of the [ ] brackets : <img src="/resources/images/array/insert/array_insert_raw.png"/></p>
				<p>There are some examples for using append method by some types.<p>
				<p><b>Integer:</b></p>
				<img src="/resources/images/array/insert/array_insert_int.png"/>
				<p><b>Double:</b></p>
				<img src="/resources/images/array/insert/array_insert_double.png"/>
				<p><b>String / Character:</b></p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>You must use double quotation marks when you append the string value. Or use single quotation marks in character value.</p>
					</div>
				</div>
				<p>String: <img src="/resources/images/array/insert/array_insert_string.png"/></p>
				<p>Character: <img src="/resources/images/array/insert/array_insert_char.png"/></p>
				<hr>
				<span><h2><a id="s-4" href="#contents">4.</a> Remove method</h2></span>
				<br>
				<p>The remove method item in flowchart removes the element at the given index (location).</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>To set the location value, it starts from '0' not at '1'. So, if you want to put a value into 1st location, you should insert '0' into 'location' part.</p>
						<p>Therefore, the location value starts from 0. (like as 0,1,2,3,4, ... )</p>
					</div>
				</div>
				<p>In this shape, you can put the location value and target name of array inside of the [ ] brackets : <img src="/resources/images/array/remove/array_remove_raw.png"/></p>
				<p>There are some examples for using append method by some types.<p>
				<p><b>Integer:</b></p>
				<img src="/resources/images/array/remove/array_remove_int.png"/>
				<p><b>Double:</b></p>
				<img src="/resources/images/array/remove/array_remove_double.png"/>
				<p><b>String / Character:</b></p>
				<p>String: <img src="/resources/images/array/remove/array_remove_string.png"/></p>
				<p>Character: <img src="/resources/images/array/remove/array_remove_char.png"/></p>
				<br><br><br><br>
			</div>
	    	<div class="col-sm-2 sidenav">
	      		<div class="well">
	        		<p>ADS</p>
	      		</div>
	      		<div class="well">
	       	 		<p>ADS</p>
	      		</div>
	    	</div>
	    </div>
	</div>
</div>

<div id="footer" class="text-center">
	<p>Designed by Janghoon Kang</p>
</div>
</body>
</html>
<!--  -->
