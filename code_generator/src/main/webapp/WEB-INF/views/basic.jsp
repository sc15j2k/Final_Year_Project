<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>
<div id="body">
	<nav class="navbar navbar-inverse navbar-custom">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
      			</button>
      			<a class="navbar-brand" href="/">Code Generator</a>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
        			<li><a href="/">Home</a></li>
        			<li><a href="#">About</a></li>
        			<li><a href="#">Contact</a></li>
      			</ul>
    		</div>
  		</div>
	</nav>

	<div class="container-fluid text-center" style="padding-bottom:100px;">
	  	<div class="row content">
	    	<div class="col-sm-2 sidenav">
	      		<h4>Code Generator Tutorial</h4>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial">Tutorial Home</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px; background-color: #4CAF50;"><a href="/tutorial/basic" class="active">Basic part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/operation">Operation part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/array">Array part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/statement">Statement part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/function">Function part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/example">Example</a></p>
	    	</div>
	    	<div class="col-sm-8 text-left">
	      		<h1>Basic part Tutorial</h1>
	      		<br>
	      		<h3><a name="contents"></a>Contents</h3>
	      		<dl name="contents" class="toc-indent">
	      			<dt><span><a href="#s-1">1</a>. Link each node</span></dt>
	      			<dt><span><a href="#s-2">2</a>. Start and End node</span></dt>
	      			<dt><span><a href="#s-3">3</a>. Variable</span></dt>
	      			<dt><span><a href="#s-4">4</a>. Equal</span></dt>
	      		</dl>
	      		<br>
	    		<span><h2><a id="s-1" href="#contents">1.</a> Link each node</h2></span>
	    		<br>
	    		<p>To build your own flowchart, You need to link each node.</p>
	    		<p>Linking between two nodes looks like this:</p>
	    		<img src="/resources/images/basic/start_end/start_end_raw.png"/>
				<hr>
				<span><h2><a id="s-2" href="#contents">2.</a>Start and End Node</h2></span>
	    		<br>
	    		<p>Start and End node represent the start and end of the main().</p>
	    		<p>Thus, except Start and End node, it cannot generate the source code.</p>
				<p>Using Start and End node looks like this:</p>
				<p>Flowchart :</p>
	    		<img src="/resources/images/basic/start_end/start_end_raw.png"/>
				<p>From this simple example, it generates the code result which related to each language.</p>
				<p>JAVA :</p>
				<img src="/resources/images/basic/start_end/start_end_java.png"/>
				<p>C :</p>
				<img src="/resources/images/basic/start_end/start_end_c.png"/>
				<br>
				<br>
				<p>However, python does not need the main() usually. So, it will not generate any code in python version.</p>
				<hr>
				<span><h2><a id="s-3" href="#contents">3.</a>Variable</h2></span>
	    		<br>
	    		<p>The variable node in flowchart generates the variable unit in the source code.</p>
	    		<p>In computer programming, a variable is a storage location (identified by a memory address) paired with an associated symbolic name (an identifier), which contains some known or unknown quantity of information referred to as a value.</p>
	    		<p>Variable types</p>
	    		<ul>
  					<li style="font-size:16px;">char: It represents the character data type. This data type saves only one letter such as 'a', '1', '?' and so on.</li>
  					<li style="font-size:16px;">int: It represents the integer data type. An integer type represents some finite set of integers in mathematics in computer science.</li>
  					<li style="font-size:16px;">double: It represents the real number data type. This data type usually saves the finite decimal number such as 1.59362.</li>
  					<li style="font-size:16px;">String: It represents the sequence of the character data type. This data type can save one or multiple words such as 'Hello World', 'Mountain' and so on.</li>
				</ul>
	    		<p>There is a list which shows the variable types supported by each language.</p>
				<ul>
  					<li style="font-size:16px;">Python: Character, Integer, Double and String</li>
  					<li style="font-size:16px;">JAVA: Character, Integer, Double and String</li>
  					<li style="font-size:16px;">C: Character, Integer ands Double</li>
				</ul>
				<br><br>
				<h2>How to use each variable type</h2>
				<p><b>Character:</b> type 'char' in the type section, type the name of the variable in the name section and type a character in the value section. You can see example image to understand easily.</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>In character type, you must use quotation marks to initialise the variable.</p>
					</div>
				</div>
				<p>Example:</p>
				<img src="/resources/images/basic/variable/variable_char.png"/>
				<br>
				<br>
				<p><b>Integer:</b> type 'int' in the type section, type the name of the variable in the name section and type the integer value in the value section. You can see example image to understand easily.</p>
				<p>Example:</p>
				<img src="/resources/images/basic/variable/variable_int.png"/>
				<br>
				<br>
				<p><b>Double:</b> type 'double' in the type section, type the name of the variable in the name section and type the double type value in the value section. You can see example image to understand easily.</p>
				<p>Example:</p>
				<img src="/resources/images/basic/variable/variable_double.png"/>
				<br>
				<br>
				<p><b>String:</b> type 'String' in the type section, type the name of the variable in the name section and type a word or multiple words in the value section. You can see example image to understand easily.</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>In string type, you must use double quotation marks to initialise the variable.</p>
					</div>
				</div>
				<p>Example:</p>
				<img src="/resources/images/basic/variable/variable_string.png"/>
				<hr>
				<span><h2><a id="s-4" href="#contents">4.</a>Equal</h2>
				<br>
				<p>The Equal item in flowchart generate the simple assignment operator in the source code.</p>
				<p>It assigns values from right side operands to left side operands.</p>
				<p>In this shape, you can edit both side operands inside of the brackets (from 'first item' and 'second item').</p>
				<img src="/resources/images/basic/equal/equal_raw.png"/>
				<p>There are examples for some variable type.</p>
				<p><b>Character :</b></p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>In character type, you must use quotation marks.</p>
					</div>
				</div>
				<img src="/resources/images/basic/equal/equal_char.png"/>
				<p><b>Integer :</b></p>
				<img src="/resources/images/basic/equal/equal_int.png"/>
				<p><b>String :</b></p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>In string type, you must use double quotation marks.</p>
					</div>
				</div>
				<img src="/resources/images/basic/equal/equal_string.png"/>
				<br><br><br><br>
	    	</div>
	    	<div class="col-sm-2 sidenav">
	      		<div class="well">
	        		<p>ADS</p>
	      		</div>
	      		<div class="well">
	       	 		<p>ADS</p>
	      		</div>
	    	</div>
	    </div>
	</div>
</div>

<div id="footer" class="text-center">
	<p>Designed by Janghoon Kang</p>
</div>
</body>
</html>
<!--  -->
