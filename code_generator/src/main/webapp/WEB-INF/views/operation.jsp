<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>
<div id="body">
	<nav class="navbar navbar-inverse navbar-custom">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
      			</button>
      			<a class="navbar-brand" href="/">Code Generator</a>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
        			<li><a href="/">Home</a></li>
        			<li><a href="#">About</a></li>
        			<li><a href="#">Contact</a></li>
      			</ul>
    		</div>
  		</div>
	</nav>

	<div class="container-fluid text-center" style="padding-bottom:100px;">
	  	<div class="row content">
	    	<div class="col-sm-2 sidenav">
	      		<h4>Code Generator Tutorial</h4>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial">Tutorial Home</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/basic">Basic part</a></p>
				<p style="line-height: 50px; margin-bottom: 0px; background-color: #4CAF50;"><a href="/tutorial/operation" class="active">Operation part</a></p>  
				<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/array">Array part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/statement">Statement part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/function">Function part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/example">Example</a></p>
	    	</div>
	    	<div class="col-sm-8 text-left">
	      		<h1>Operation part Tutorial</h1>
	      		<br>
	      		<h3><a name="contents"></a>Contents</h3>
	      		<dl name="contents" class="toc-indent">
	      			<dt><span><a href="#s-1">1</a>. Addition</span></dt>
	      			<dt><span><a href="#s-2">2</a>. Substraction</span></dt>
	      			<dt><span><a href="#s-3">3</a>. Multiplication</span></dt>
					<dt><span><a href="#s-4">4</a>. Division</span></dt>
					<dt><span><a href="#s-5">5</a>. Remainder</span></dt>
	      		</dl>
	      		<br>
	    		<span><h2><a id="s-1" href="#contents">1.</a> Addition</h2></span>
	    		<br>
				<p>Adds values on either side of the operator.</p>
				<p>In detail, the result of addition between 'second item' and 'third item' part will save into 'first item' part</p>
				<p>You must unify the same data type when you want to add between two different values or variables.</p>
				<p>In this shape, you can edit three side operands inside of the brackets (from 'first item', 'second item' and 'third item').</p>
	    		<p>The initial addition object looks like this:</p>
				<img src="/resources/images/operation/addition/addition_raw.png"/>
				<br>
				<h2>How to use addition operation</h2>
				<p>The format of this addition object follows the syntax of the programming language.</p>
				<p>There are some examples for using the addition operation.</p>
				<p>Case 1:</p>
				<p>In this case, I stored the result of the addition of the variable i and the variable j in the 'result' variable.</p>
				<img src="/resources/images/operation/addition/addition_case1.png"/>
				<p>Case 2:</p>
				<p>In this case, I stored the result of the addition between 100 and 4 in the 'result' variable.</p>
				<img src="/resources/images/operation/addition/addition_case2.png"/>
				<hr>
				<span><h2><a id="s-2" href="#contents">2.</a> Substraction</h2></span>
	    		<br>
	    		<p>Subtracts right hand operand from left hand operand.</p>
	    		<p>In detail, the result of substraction between 'second item' and 'third item' part will save into 'first item' part</p>
				<p>You must unify the same data type when you want to substract between two different values or variables.</p>
				<p>In this shape, you can edit three side operands inside of the brackets (from 'first item', 'second item' and 'third item').</p>
	    		<p>The initial substraction object looks like this:</p>
				<img src="/resources/images/operation/substraction/substraction_raw.png"/>
				<br>
				<h2>How to use substraction operation</h2>
				<p>The format of this substraction object follows the syntax of the programming language.</p>
				<p>There are some examples for using the substraction operation.</p>
				<p>Case 1:</p>
				<p>In this case, I stored the result of the substraction of the variable i and the variable j in the 'result' variable.</p>
				<img src="/resources/images/operation/substraction/substraction_case1.png"/>
				<p>Case 2:</p>
				<p>In this case, I stored the result of the substraction between 100 and 30 in the 'result' variable.</p>
				<img src="/resources/images/operation/substraction/substraction_case2.png"/>
				<hr>
				<span><h2><a id="s-3" href="#contents">3.</a> Multiplication</h2></span>
	    		<br>
	    		<p>Multiplies values on either side of the operator.</p>
	    		<p>In detail, the result of multiplication between 'second item' and 'third item' part will save into 'first item' part</p>
				<p>You must unify the same data type when you want to multiply between two different values or variables.</p>
				<p>In this shape, you can edit three side operands inside of the brackets (from 'first item', 'second item' and 'third item').</p>
	    		<p>The initial multiplication object looks like this:</p>
				<img src="/resources/images/operation/multiplication/multiplication_raw.png"/>
				<br>
				<h2>How to use multiplication operation</h2>
				<p>The format of this multiplication object follows the syntax of the programming language.</p>
				<p>There are some examples for using the multiplication operation.</p>
				<p>Case 1:</p>
				<p>In this case, I stored the result of the multiplication of the variable i and the variable j in the 'result' variable.</p>
				<img src="/resources/images/operation/multiplication/multiplication_case1.png"/>
				<p>Case 2:</p>
				<p>In this case, I stored the result of the multiplication between 57 and 139 in the 'result' variable.</p>
				<img src="/resources/images/operation/multiplication/multiplication_case2.png"/>
				<hr>
				<span><h2><a id="s-4" href="#contents">4.</a> Division</h2></span>
	    		<br>
	    		<p>Divides left hand operand by right hand operand.</p>
	    		<p>In detail, the result of division between 'second item' and 'third item' part will save into 'first item' part</p>
				<p>You must unify the same data type when you want to divide between two different values or variables.</p>
				<p>In this shape, you can edit three side operands inside of the brackets (from 'first item', 'second item' and 'third item').</p>
	    		<p>The initial division object looks like this:</p>
				<img src="/resources/images/operation/division/division_raw.png"/>
				<br>
				<h2>How to use division operation</h2>
				<p>The format of this division object follows the syntax of the programming language.</p>
				<p>There are some examples for using the division operation.</p>
				<p>Case 1:</p>
				<p>In this case, I stored the result of the division of the variable i and the variable j in the 'result' variable.</p>
				<img src="/resources/images/operation/division/division_case1.png"/>
				<p>Case 2:</p>
				<p>In this case, I stored the result of the division between 123 and 7 in the 'result' variable.</p>
				<img src="/resources/images/operation/division/division_case2.png"/>
				<hr>
				<span><h2><a id="s-5" href="#contents">5.</a> Remainder</h2></span>
	    		<br>
	    		<p>Returns the remainder which is from the division between left hand operand and right hand operand.</p>
				<p>In detail, the result of Remainder between 'second item' and 'third item' part will save into 'first item' part</p>
				<p>You must unify the same data type between two different values or variables.</p>
				<p>Besides, you must use integer data type when you want to save remainder value.</p>
				<p>In this shape, you can edit three side operands inside of the brackets (from 'first item', 'second item' and 'third item').</p>
	    		<p>The initial remainder object looks like this:</p>
				<img src="/resources/images/operation/remainder/remainder_raw.png"/>
				<br>
				<h2>How to use remainder operation</h2>
				<p>The format of this remainder object follows the syntax of the programming language.</p>
				<p>There are some examples for using the remainder operation.</p>
				<p>Case 1:</p>
				<p>In this case, I stored the result of the remainder of the variable i and the variable j in the 'result' variable.</p>
				<img src="/resources/images/operation/remainder/remainder_case1.png"/>
				<p>Case 2:</p>
				<p>In this case, I stored the result of the remainder between 57 and 139 in the 'result' variable.</p>
				<img src="/resources/images/operation/remainder/remainder_case2.png"/>
				<br><br><br><br>
	    	</div>
	    	<div class="col-sm-2 sidenav">
	      		<div class="well">
	        		<p>ADS</p>
	      		</div>
	      		<div class="well">
	       	 		<p>ADS</p>
	      		</div>
	    	</div>
	    </div>
	</div>
</div>

<div id="footer" class="text-center">
	<p>Designed by Janghoon Kang</p>
</div>
</body>
</html>
<!--  -->
