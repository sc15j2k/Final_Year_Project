<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script id="code">
    function init() {
        var $ = go.GraphObject.make;  // for conciseness in defining templates
        myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
            initialContentAlignment: go.Spot.Center,
            allowDrop: true,  // must be true to accept drops from the Palette
            "LinkDrawn": showLinkLabel,  // this DiagramEvent listener is defined below
            "LinkRelinked": showLinkLabel,
            scrollsPageOnFocus: false,
            "undoManager.isEnabled": true  // enable undo & redo
        });
        myDiagram.autoScale = go.Diagram.Uniform;
        // when the document is modified, add a "*" to the title and enable the "Save" button
        myDiagram.addDiagramListener("Modified", function(e) {
            var button = document.getElementById("SaveButton");
            if (button) button.disabled = !myDiagram.isModified;
            var idx = document.title.indexOf("*");
            if (myDiagram.isModified) {
                if (idx < 0) document.title += "*";
            } else {
                if (idx >= 0) document.title = document.title.substr(0, idx);
            }
        });
        // helper definitions for node templates
        function nodeStyle() {
            return [
                // The Node.location comes from the "loc" property of the node data,
                // converted by the Point.parse static method.
                // If the Node.location is changed, it updates the "loc" property of the node data,
                // converting back using the Point.stringify static method.
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                {
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center,
                //isShadowed: true,
                //shadowColor: "#888",
                // handle mouse enter/leave events to show/hide the ports
                mouseEnter: function (e, obj) { showPorts(obj.part, true); },
                mouseLeave: function (e, obj) { showPorts(obj.part, false); }
                }
            ];
        }
        // Define a function for creating a "port" that is normally transparent.
        // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
        // and where the port is positioned on the node, and the boolean "output" and "input" arguments
        // control whether the user can draw links from or to the port.
        function makePort_if(name, portName, leftside) {
            var port = $(go.Shape, "Rectangle",
                        {
                            fill: "gray", stroke: null,
                            desiredSize: new go.Size(8, 8),
                            portId: portName,  // declare this object to be a "port"
                            toMaxLinks: 1,  // don't allow more than one link into a port
                            cursor: "pointer"  // show a different cursor to indicate potential link point
                        });

            var lab = $(go.TextBlock, name,  // the name of the port
                        { font: "12pt sans-serif" });

            var panel = $(go.Panel, "Horizontal",
                            { margin: new go.Margin(2, 0) });

            // set up the port/panel based on which side of the node it will be on
            if (leftside) {
                port.fromSpot = go.Spot.Left;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 0, 0, 1);
                panel.alignment = go.Spot.Left;
                panel.add(port);
                panel.add(lab);
            } else {
                port.fromSpot = go.Spot.Right;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 1, 0, 0);
                panel.alignment = go.Spot.Right;
                panel.add(lab);
                panel.add(port);
            }
            return panel;
        }
        
        function makePort_while_for(name, portName) {
            var port = $(go.Shape, "Rectangle",
                        {
                            fill: "gray", stroke: null,
                            desiredSize: new go.Size(8, 8),
                            portId: portName,  // declare this object to be a "port"
                            toMaxLinks: 1,  // don't allow more than one link into a port
                            cursor: "pointer"  // show a different cursor to indicate potential link point
                        });

            var lab = $(go.TextBlock, name,  // the name of the port
                        { font: "12pt sans-serif" });

            var panel = $(go.Panel, "Horizontal",
                            { margin: new go.Margin(2, 0) });
            var panel2 = $(go.Panel, "Vertical",
                            { margin: new go.Margin(2, 0) });

            // set up the port/panel based on which side of the node it will be on
            if (portName == "B") {
                port.fromSpot = go.Spot.Bottom;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 0, 1, 0);
                panel.alignment = go.Spot.Bottom;
                panel.add(port);
                panel.add(lab);
            }else if (portName == "L") {
                port.toSpot = go.Spot.Left;
                port.toLinkable = true;
                lab.margin = new go.Margin(1, 0, 0, 1);
                panel.alignment = go.Spot.Left;
                panel.add(port);
                panel.add(lab);
            }else if (portName == "R") {
                port.fromSpot = go.Spot.Right;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 1, 0, 0);
                panel.alignment = go.Spot.Right;
                panel.add(lab);
                panel.add(port);
            }
            return panel;
        }
        
        function makePort_method(name, portName) {
            var port = $(go.Shape, "Rectangle",
                        {
                            fill: "gray", stroke: null,
                            desiredSize: new go.Size(8, 8),
                            portId: portName,  // declare this object to be a "port"
                            toMaxLinks: 1,  // don't allow more than one link into a port
                            cursor: "pointer"  // show a different cursor to indicate potential link point
                        });

            var lab = $(go.TextBlock, name,  // the name of the port
                        { font: "12pt sans-serif" });

            var panel = $(go.Panel, "Horizontal",
                            { margin: new go.Margin(2, 0) });
            var panel2 = $(go.Panel, "Vertical",
                            { margin: new go.Margin(2, 0) });
            // set up the port/panel based on which side of the node it will be on
            if (portName == "B") {
                port.fromSpot = go.Spot.Bottom;
                port.toLinkable = true;
                lab.margin = new go.Margin(1, 0, -1, 0);
                panel.alignment = go.Spot.Bottom;
                panel.add(lab);
                panel.add(port);
            }else if (portName == "L") {
                port.toSpot = go.Spot.Left;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 0, 0, 1);
                panel.alignment = go.Spot.Left;
                panel.add(port);
                panel.add(lab);
            }else if (portName == "R") {
                port.fromSpot = go.Spot.Right;
                port.fromLinkable = true;
                lab.margin = new go.Margin(1, 1, 0, 0);
                panel.alignment = go.Spot.Right;
                panel.add(lab);
                panel.add(port);
            }
            return panel;
        }

        
        
        function makePort(name, spot, output, input) {
        // the port is basically just a small circle that has a white stroke when it is made visible
            return $(go.Shape, "Circle",
                {
                    fill: "transparent",
                    stroke: null,  // this is changed to "white" in the showPorts function
                    desiredSize: new go.Size(8, 8),
                    alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                    portId: name,  // declare this object to be a "port"
                    fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                    fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                    cursor: "pointer"  // show a different cursor to indicate potential link point
                });
        }
        // define the Node templates for regular nodes
        var lightText = 'whitesmoke';
        myDiagram.nodeTemplateMap.add("",  // the default category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Rectangle",
                { fill: "#00A9C9", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                $(go.TextBlock,{
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 8,
                    maxSize: new go.Size(160, NaN),
                    wrap: go.TextBlock.WrapFit,
                    editable: true
                },new go.Binding("text").makeTwoWay())
                )
            ),
            // four named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort("B", go.Spot.Bottom, true, false)
        ));
        myDiagram.nodeTemplateMap.add("Start",
        $(go.Node, "Spot", nodeStyle(),
            $(go.Panel, "Auto",
            $(go.Shape, "Circle",
                { minSize: new go.Size(40, 40), fill: "#79C900", stroke: null }),
            $(go.TextBlock, "Start",
                { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
                new go.Binding("text"))
            ),
            // three named ports, one on each side except the top, all output only:
            makePort("L", go.Spot.Left, true, false),
            makePort("R", go.Spot.Right, true, false),
            makePort("B", go.Spot.Bottom, true, false)
        ));
        myDiagram.nodeTemplateMap.add("End",
        $(go.Node, "Spot", nodeStyle(),
            $(go.Panel, "Auto",
            $(go.Shape, "Circle",
                { minSize: new go.Size(40, 40), fill: "#DC3C00", stroke: null }),
            $(go.TextBlock, "End",
                { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
                new go.Binding("text"))
            ),
            // three named ports, one on each side except the bottom, all input only:
            makePort("T", go.Spot.Top, false, true),
            makePort("L", go.Spot.Left, false, true),
            makePort("R", go.Spot.Right, false, true)
        ));
        
        myDiagram.nodeTemplateMap.add("Equal",  // the print category
                $(go.Node, "Spot", nodeStyle(),
                    // the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
	                    $(go.Shape, "SquareArrow",
	                        { fill: "SkyBlue", stroke: null },
	                        new go.Binding("figure", "figure")
	                    ),
	                    $(go.Panel, "Vertical",
		                    $(go.TextBlock, 
		                        new go.Binding("text", "category"),
		                        new go.Binding("background", "fill"),{ 
		                        font: "bold 11pt Helvetica, Arial, sans-serif",
		                        margin: 2,
		                        isMultiline: false }),
		                    $(go.Panel,"Horizontal",
		                    	$(go.TextBlock,{ 
				                    font: "bold 11pt Helvetica, Arial, sans-serif",
				                    margin: 2,
				                    text:"[",
				                    isMultiline: false }),
		                      	$(go.TextBlock,{
				                        font: "bold 11pt Helvetica, Arial, sans-serif",
				                        margin: 8,
				                        maxSize: new go.Size(160, NaN),
				                        wrap: go.TextBlock.WrapFit,
				                        editable: true
				                      	},new go.Binding("text","first").makeTwoWay()),
				                      	$(go.TextBlock,{ 
						                        font: "bold 11pt Helvetica, Arial, sans-serif",
						                        margin: 2,
						                        text:"] = [",
						                        isMultiline: false }),
				                      	$(go.TextBlock,{
					                        font: "bold 11pt Helvetica, Arial, sans-serif",
					                        margin: 8,
					                        maxSize: new go.Size(160, NaN),
					                        wrap: go.TextBlock.WrapFit,
					                        editable: true
					                      	},new go.Binding("text","second").makeTwoWay()),
					                   $(go.TextBlock,{ 
						               		font: "bold 11pt Helvetica, Arial, sans-serif",
						                    margin: 2,
						                    text:"]",
						                    isMultiline: false })
				                      	
		                    )
		                )
	                ),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
                ));
        
        myDiagram.nodeTemplateMap.add("Variable",  // the variable category
        	$(go.Node, "Spot", nodeStyle(),
            	// the main object is a Panel that surrounds a TextBlock with a output Shape
                $(go.Panel, "Auto",
	                $(go.Shape, "Rectangle",
	                	{ fill: "SkyBlue", stroke: null },
	                    new go.Binding("figure", "figure")
	                ),
	                $(go.Panel, "Vertical",
		                $(go.TextBlock, 
		                    new go.Binding("text", "category"),
		                    new go.Binding("background", "fill"),{ 
		                    font: "bold 11pt Helvetica, Arial, sans-serif",
		                    margin: 2,
		                    isMultiline: false }),
		                $(go.Panel,"Horizontal",
	                		$(go.TextBlock,{ 
				            	font: "bold 11pt Helvetica, Arial, sans-serif",
				            	margin: 2,
				                text:"",
				                isMultiline: false }),
		                	$(go.TextBlock,{ 
				            	font: "bold 11pt Helvetica, Arial, sans-serif",
				            	margin: 0,
				                text:"[",
				                isMultiline: false }),
		                    $(go.TextBlock,{
				            	font: "bold 11pt Helvetica, Arial, sans-serif",
				                margin: 0,
				                maxSize: new go.Size(160, NaN),
				                wrap: go.TextBlock.WrapFit,
				                editable: false
				                },new go.Binding("text","text")),
				            $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"] [",
						        isMultiline: false }),
							$(go.TextBlock,{
					        	font: "bold 11pt Helvetica, Arial, sans-serif",
					            margin: 1,
					            maxSize: new go.Size(160, NaN),
					            wrap: go.TextBlock.WrapFit,
					            editable: true
					            },new go.Binding("text","first").makeTwoWay()),
					        $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"] [",
						        isMultiline: false }),
						    $(go.TextBlock,{
							    font: "bold 11pt Helvetica, Arial, sans-serif",
							    margin: 0,
							    maxSize: new go.Size(160, NaN),
							    wrap: go.TextBlock.WrapFit,
							    editable: true
							    },new go.Binding("text","second").makeTwoWay()),
							$(go.TextBlock,{ 
								font: "bold 11pt Helvetica, Arial, sans-serif",
								margin: 0,
								text:"]",
								isMultiline: false }),
							$(go.TextBlock,{ 
				            	font: "bold 11pt Helvetica, Arial, sans-serif",
				            	margin: 2,
				                text:"",
				                isMultiline: false })
		            	)
		        	)
	        	),
                // two named ports, one on each side:
                makePort("T", go.Spot.Top, false, true),
                makePort("B", go.Spot.Bottom, true, false)
        	)
        );
        
        myDiagram.nodeTemplateMap.add("Addition",  // the addition category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#deb887", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"]=[",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        						    margin: 0,
        						    text:"]+[",
        						    isMultiline: false }),
        						$(go.TextBlock,{
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							maxSize: new go.Size(160, NaN),
        							wrap: go.TextBlock.WrapFit,
        							editable: true
        							},new go.Binding("text","third").makeTwoWay()),
        						$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							text:"]",
        							isMultiline: false }),
        						$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Substraction",  // the substraction category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#deb887", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"]=[",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        						    margin: 0,
        						    text:"]-[",
        						    isMultiline: false }),
        						$(go.TextBlock,{
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							maxSize: new go.Size(160, NaN),
        							wrap: go.TextBlock.WrapFit,
        							editable: true
        							},new go.Binding("text","third").makeTwoWay()),
        						$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							text:"]",
        							isMultiline: false }),
        						$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Multiplication",  // the multiplication category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#deb887", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"]=[",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        						    margin: 0,
        						    text:"]x[",
        						    isMultiline: false }),
        						$(go.TextBlock,{
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							maxSize: new go.Size(160, NaN),
        							wrap: go.TextBlock.WrapFit,
        							editable: true
        							},new go.Binding("text","third").makeTwoWay()),
        						$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							text:"]",
        							isMultiline: false }),
        						$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Division",  // the division category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#deb887", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"]=[",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        						    margin: 0,
        						    text:"]÷[",
        						    isMultiline: false }),
        						$(go.TextBlock,{
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							maxSize: new go.Size(160, NaN),
        							wrap: go.TextBlock.WrapFit,
        							editable: true
        							},new go.Binding("text","third").makeTwoWay()),
        						$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							text:"]",
        							isMultiline: false }),
        						$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Remainder",  // the remainder category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#deb887", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"]=[",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        						    margin: 0,
        						    text:"]%[",
        						    isMultiline: false }),
        						$(go.TextBlock,{
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							maxSize: new go.Size(160, NaN),
        							wrap: go.TextBlock.WrapFit,
        							editable: true
        							},new go.Binding("text","third").makeTwoWay()),
        						$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							text:"]",
        							isMultiline: false }),
        						$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Array",  // the array category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#fa6e79", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				                margin: 0,
    				                maxSize: new go.Size(160, NaN),
    				                wrap: go.TextBlock.WrapFit,
    				                editable: false
    				                },new go.Binding("text","text")),
    				            $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:": [",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"][",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
    								font: "bold 11pt Helvetica, Arial, sans-serif",
    								margin: 0,
    								text:"]",
    								isMultiline: false }),
    							$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Append",  // the append category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#fa6e79", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"][",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
    								font: "bold 11pt Helvetica, Arial, sans-serif",
    								margin: 0,
    								text:"]",
    								isMultiline: false }),
    							$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Insert",  // the insert category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#fa6e79", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"][",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        						    margin: 0,
        						    text:"][",
        						    isMultiline: false }),
        						$(go.TextBlock,{
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							maxSize: new go.Size(160, NaN),
        							wrap: go.TextBlock.WrapFit,
        							editable: true
        							},new go.Binding("text","third").makeTwoWay()),
        						$(go.TextBlock,{ 
        							font: "bold 11pt Helvetica, Arial, sans-serif",
        							margin: 0,
        							text:"]",
        							isMultiline: false }),
        						$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
        myDiagram.nodeTemplateMap.add("Remove",  // the remove category
            	$(go.Node, "Spot", nodeStyle(),
                	// the main object is a Panel that surrounds a TextBlock with a output Shape
                    $(go.Panel, "Auto",
    	                $(go.Shape, "Rectangle",
    	                	{ fill: "#fa6e79", stroke: null },
    	                    new go.Binding("figure", "figure")
    	                ),
    	                $(go.Panel, "Vertical",
    		                $(go.TextBlock, 
    		                    new go.Binding("text", "category"),
    		                    new go.Binding("background", "fill"),{ 
    		                    font: "bold 11pt Helvetica, Arial, sans-serif",
    		                    margin: 2,
    		                    isMultiline: false }),
    		                $(go.Panel,"Horizontal",
    	                		$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false }),
    		                	$(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"[",
    						        isMultiline: false }),
    							$(go.TextBlock,{
    					        	font: "bold 11pt Helvetica, Arial, sans-serif",
    					            margin: 1,
    					            maxSize: new go.Size(160, NaN),
    					            wrap: go.TextBlock.WrapFit,
    					            editable: true
    					            },new go.Binding("text","first").makeTwoWay()),
    					        $(go.TextBlock,{ 
    						    	font: "bold 11pt Helvetica, Arial, sans-serif",
    						        margin: 0,
    						        text:"][",
    						        isMultiline: false }),
    						    $(go.TextBlock,{
    							    font: "bold 11pt Helvetica, Arial, sans-serif",
    							    margin: 0,
    							    maxSize: new go.Size(160, NaN),
    							    wrap: go.TextBlock.WrapFit,
    							    editable: true
    							    },new go.Binding("text","second").makeTwoWay()),
    							$(go.TextBlock,{ 
    								font: "bold 11pt Helvetica, Arial, sans-serif",
    								margin: 0,
    								text:"]",
    								isMultiline: false }),
    							$(go.TextBlock,{ 
    				            	font: "bold 11pt Helvetica, Arial, sans-serif",
    				            	margin: 2,
    				                text:"",
    				                isMultiline: false })
    		            	)
    		        	)
    	        	),
                    // two named ports, one on each side:
                    makePort("T", go.Spot.Top, false, true),
                    makePort("B", go.Spot.Bottom, true, false)
            	)
            );
        
 		myDiagram.nodeTemplateMap.add("If",  // the if category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a diamond Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Diamond",
                { fill: "#E88800", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                    $(go.Panel,"Horizontal",
	                	$(go.TextBlock,{ 
				           	font: "bold 11pt Helvetica, Arial, sans-serif",
				           	margin: 2,
				            text:"",
				            isMultiline: false }),
		                $(go.TextBlock,{
					      	font: "bold 11pt Helvetica, Arial, sans-serif",
					        margin: 1,
					        maxSize: new go.Size(160, NaN),
					        wrap: go.TextBlock.WrapFit,
					        editable: true
					        },new go.Binding("text","text")),
					    $(go.TextBlock,{ 
						  	font: "bold 11pt Helvetica, Arial, sans-serif",
						    margin: 0,
						    text:" [",
						    isMultiline: false }),
						$(go.TextBlock,{
						    font: "bold 11pt Helvetica, Arial, sans-serif",
							margin: 0,
						    maxSize: new go.Size(160, NaN),
						    wrap: go.TextBlock.WrapFit,
						    editable: true
						    },new go.Binding("text","first").makeTwoWay()),
						$(go.TextBlock,{ 
							font: "bold 11pt Helvetica, Arial, sans-serif",
							margin: 0,
							text:"]",
							isMultiline: false }),
						$(go.TextBlock,{ 
				           	font: "bold 11pt Helvetica, Arial, sans-serif",
				           	margin: 2,
				        	text:"",
				            isMultiline: false })
		            	)
		        	)
	        	),
            // three named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort_if("True","L",true),
            makePort_if("False","R",false)
        ));
 		myDiagram.nodeTemplateMap.add("End if",
 		$(go.Node, "Spot", nodeStyle(),
 			$(go.Panel, "Auto",
 		    $(go.Shape, "Circle",
 		    	{ minSize: new go.Size(40, 40), fill: "#E88800", stroke: null }),
 		    $(go.TextBlock, "End if",
 		        { font: "bold 11pt Helvetica, Arial, sans-serif" },
 		        new go.Binding("text"))
 		    ),
 		    // three named ports, one on each side except the bottom, all input only:
 		    makePort("T", go.Spot.Top, false, true),
 		   	makePort("B", go.Spot.Bottom, true, false)
 		));
 		myDiagram.nodeTemplateMap.add("Print",  // the print category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a output Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Output",
                { fill: "#0BFFC8", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                    $(go.Panel,"Horizontal",
    	                	$(go.TextBlock,{ 
    				           	font: "bold 11pt Helvetica, Arial, sans-serif",
    				           	margin: 2,
    				            text:"",
    				            isMultiline: false }),
    		                $(go.TextBlock,{
    					      	font: "bold 11pt Helvetica, Arial, sans-serif",
    					        margin: 1,
    					        maxSize: new go.Size(160, NaN),
    					        wrap: go.TextBlock.WrapFit,
    					        editable: true
    					        },new go.Binding("text","text")),
    					    $(go.TextBlock,{ 
    						  	font: "bold 11pt Helvetica, Arial, sans-serif",
    						    margin: 0,
    						    text:" [",
    						    isMultiline: false }),
    						$(go.TextBlock,{
    						    font: "bold 11pt Helvetica, Arial, sans-serif",
    							margin: 0,
    						    maxSize: new go.Size(160, NaN),
    						    wrap: go.TextBlock.WrapFit,
    						    editable: true
    						    },new go.Binding("text","first").makeTwoWay()),
    						$(go.TextBlock,{ 
    							font: "bold 11pt Helvetica, Arial, sans-serif",
    							margin: 0,
    							text:"]",
    							isMultiline: false }),
    						$(go.TextBlock,{ 
    				           	font: "bold 11pt Helvetica, Arial, sans-serif",
    				           	margin: 2,
    				        	text:"",
    				            isMultiline: false })
    		            	)
    		        	)
    	        	),
            // two named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort("B", go.Spot.Bottom, true, false)
        ));
 		myDiagram.nodeTemplateMap.add("Method",  // the method category
        $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
            $(go.Panel, "Auto",
            $(go.Shape, "ExternalProcess",
                { fill: "#00D3FF", stroke: null },
                new go.Binding("figure", "figure")),
                $(go.Panel, "Vertical",
                $(go.TextBlock, 
                    new go.Binding("text", "category"),
                    new go.Binding("background", "fill"),{ 
                    font: "bold 11pt Helvetica, Arial, sans-serif",
                    margin: 2,
                    isMultiline: false }),
                    $(go.Panel,"Horizontal",
	                		$(go.TextBlock,{ 
				            	font: "bold 11pt Helvetica, Arial, sans-serif",
				            	margin: 2,
				                text:"",
				                isMultiline: false }),
				            $(go.TextBlock,{
						       	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 1,
						        maxSize: new go.Size(160, NaN),
						        wrap: go.TextBlock.WrapFit,
						        editable: true
						        },new go.Binding("text","text").makeTwoWay()),
						    $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"[",
						        isMultiline: false }),
							$(go.TextBlock,{
					        	font: "bold 11pt Helvetica, Arial, sans-serif",
					            margin: 1,
					            maxSize: new go.Size(160, NaN),
					            wrap: go.TextBlock.WrapFit,
					            editable: true
					            },new go.Binding("text","first").makeTwoWay()),
					        $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"]",
						        isMultiline: false })
	                ),
	                $(go.Panel,"Horizontal",
	                		$(go.TextBlock,{ 
				            	font: "bold 11pt Helvetica, Arial, sans-serif",
				            	margin: 2,
				                text:"",
				                isMultiline: false }),
				            $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"[",
						        isMultiline: false }),
							$(go.TextBlock,{
					        	font: "bold 11pt Helvetica, Arial, sans-serif",
					            margin: 1,
					            maxSize: new go.Size(160, NaN),
					            wrap: go.TextBlock.WrapFit,
					            editable: true
					            },new go.Binding("text","second").makeTwoWay()),
					        $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"]",
						        isMultiline: false })
						    		
	                ),
	                $(go.Panel,"Horizontal",
	                		$(go.TextBlock,{ 
				            	font: "bold 11pt Helvetica, Arial, sans-serif",
				            	margin: 2,
				                text:"",
				                isMultiline: false }),
				            $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"[",
						        isMultiline: false }),
							$(go.TextBlock,{
					        	font: "bold 11pt Helvetica, Arial, sans-serif",
					            margin: 1,
					            maxSize: new go.Size(160, NaN),
					            wrap: go.TextBlock.WrapFit,
					            editable: true
					            },new go.Binding("text","third").makeTwoWay()),
					        $(go.TextBlock,{ 
						    	font: "bold 11pt Helvetica, Arial, sans-serif",
						        margin: 0,
						        text:"]",
						        isMultiline: false })
	                )
	        	)
        	),
            // four named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort_method("Continue","R"),
            makePort_method("Finishing\nMethod","B"),
            makePort_method("Escape","L")
        ));
 		myDiagram.nodeTemplateMap.add("Return",  // the return category
 		$(go.Node, "Spot", nodeStyle(),
 			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
 		    $(go.Panel, "Auto",
 		    $(go.Shape, "Rectangle",
 		    	{ fill: "#00D3FF", stroke: null },
 		    	new go.Binding("figure", "figure")),
 		    	$(go.Panel, "Vertical",
 		        $(go.TextBlock, 
 		        	new go.Binding("text", "category"),
 		        	new go.Binding("background", "fill"),{ 
 		            font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 2,
 		            isMultiline: false }),
 		           $(go.Panel,"Horizontal",
 		                	$(go.TextBlock,{ 
 					           	font: "bold 11pt Helvetica, Arial, sans-serif",
 					           	margin: 2,
 					            text:"",
 					            isMultiline: false }),
 			                $(go.TextBlock,{
 						      	font: "bold 11pt Helvetica, Arial, sans-serif",
 						        margin: 1,
 						        maxSize: new go.Size(160, NaN),
 						        wrap: go.TextBlock.WrapFit,
 						        editable: true
 						        },new go.Binding("text","text")),
 						    $(go.TextBlock,{ 
 							  	font: "bold 11pt Helvetica, Arial, sans-serif",
 							    margin: 0,
 							    text:" [",
 							    isMultiline: false }),
 							$(go.TextBlock,{
 							    font: "bold 11pt Helvetica, Arial, sans-serif",
 								margin: 0,
 							    maxSize: new go.Size(160, NaN),
 							    wrap: go.TextBlock.WrapFit,
 							    editable: true
 							    },new go.Binding("text","first").makeTwoWay()),
 							$(go.TextBlock,{ 
 								font: "bold 11pt Helvetica, Arial, sans-serif",
 								margin: 0,
 								text:"]",
 								isMultiline: false }),
 							$(go.TextBlock,{ 
 					           	font: "bold 11pt Helvetica, Arial, sans-serif",
 					           	margin: 2,
 					        	text:"",
 					            isMultiline: false })
 			            	)
 			        	)
 		        	),
 		    // two named ports, one on each side:
 		    makePort("T", go.Spot.Top, false, true),
 		    makePort("B", go.Spot.Bottom, true, false)
 		));
 		myDiagram.nodeTemplateMap.add("For",  // the for category
 		$(go.Node, "Spot", nodeStyle(),
 			// the main object is a Panel that surrounds a TextBlock with a diamond Shape
 		    $(go.Panel, "Auto",
 		    $(go.Shape, "Diamond",
 		    	{ fill: "#FF22FC", stroke: null, minSize: new go.Size(200,30) },
 		        new go.Binding("figure", "figure")),
 		       $(go.Panel, "Vertical",
		                $(go.TextBlock, 
		                    new go.Binding("text", "category"),
		                    new go.Binding("background", "fill"),{ 
		                    font: "bold 11pt Helvetica, Arial, sans-serif",
		                    margin: 2,
		                    isMultiline: false }),
		                $(go.Panel,"Horizontal",
		                		$(go.TextBlock,{ 
					            	font: "bold 11pt Helvetica, Arial, sans-serif",
					            	margin: 2,
					                text:"",
					                isMultiline: false }),
					            $(go.TextBlock,{
							       	font: "bold 11pt Helvetica, Arial, sans-serif",
							        margin: 1,
							        maxSize: new go.Size(160, NaN),
							        wrap: go.TextBlock.WrapFit,
							        editable: true
							        },new go.Binding("text","text").makeTwoWay()),
							    $(go.TextBlock,{ 
							    	font: "bold 11pt Helvetica, Arial, sans-serif",
							        margin: 0,
							        text:"[",
							        isMultiline: false }),
								$(go.TextBlock,{
						        	font: "bold 11pt Helvetica, Arial, sans-serif",
						            margin: 1,
						            maxSize: new go.Size(160, NaN),
						            wrap: go.TextBlock.WrapFit,
						            editable: true
						            },new go.Binding("text","first").makeTwoWay()),
						        $(go.TextBlock,{ 
							    	font: "bold 11pt Helvetica, Arial, sans-serif",
							        margin: 0,
							        text:"]",
							        isMultiline: false }),
							    
		                ),
		                $(go.Panel,"Horizontal",
		                		$(go.TextBlock,{ 
					            	font: "bold 11pt Helvetica, Arial, sans-serif",
					            	margin: 2,
					                text:"",
					                isMultiline: false }),
					            $(go.TextBlock,{ 
							    	font: "bold 11pt Helvetica, Arial, sans-serif",
							        margin: 0,
							        text:"to [",
							        isMultiline: false }),
								$(go.TextBlock,{
						        	font: "bold 11pt Helvetica, Arial, sans-serif",
						            margin: 1,
						            maxSize: new go.Size(160, NaN),
						            wrap: go.TextBlock.WrapFit,
						            editable: true
						            },new go.Binding("text","second").makeTwoWay()),
						        $(go.TextBlock,{ 
							    	font: "bold 11pt Helvetica, Arial, sans-serif",
							        margin: 0,
							        text:"]",
							        isMultiline: false }),
							    		
		                ),
		                $(go.Panel,"Horizontal",
		                		$(go.TextBlock,{ 
					            	font: "bold 11pt Helvetica, Arial, sans-serif",
					            	margin: 2,
					                text:"",
					                isMultiline: false }),
					            $(go.TextBlock,{ 
							    	font: "bold 11pt Helvetica, Arial, sans-serif",
							        margin: 0,
							        text:"step [",
							        isMultiline: false }),
								$(go.TextBlock,{
						        	font: "bold 11pt Helvetica, Arial, sans-serif",
						            margin: 1,
						            maxSize: new go.Size(160, NaN),
						            wrap: go.TextBlock.WrapFit,
						            editable: true
						            },new go.Binding("text","third").makeTwoWay()),
						        $(go.TextBlock,{ 
							    	font: "bold 11pt Helvetica, Arial, sans-serif",
							        margin: 0,
							        text:"]",
							        isMultiline: false })
		                )
		        	)
	        	),
 		    // three named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort_while_for("Finish For","L"),
            makePort_while_for("Escape For","R"),
            makePort_while_for("True","B")
		));
 		myDiagram.nodeTemplateMap.add("While",  // the while category
 		$(go.Node, "Spot", nodeStyle(),
 			// the main object is a Panel that surrounds a TextBlock with a diamond Shape
 		    $(go.Panel, "Auto",
 		    $(go.Shape, "Diamond",
 		    	{ fill: "#FF2476", stroke: null, minSize: new go.Size(180,30) },
 		        new go.Binding("figure", "figure")),
 		        $(go.Panel, "Vertical",
 		        $(go.TextBlock, 
 		        	new go.Binding("text", "category"),
 		            new go.Binding("background", "fill"),{ 
 		            font: "bold 11pt Helvetica, Arial, sans-serif",
 		            margin: 2,
 		            isMultiline: false }),
 		            $(go.Panel,"Horizontal",
 		                	$(go.TextBlock,{ 
 					           	font: "bold 11pt Helvetica, Arial, sans-serif",
 					           	margin: 2,
 					            text:"",
 					            isMultiline: false }),
 			                $(go.TextBlock,{
 						      	font: "bold 11pt Helvetica, Arial, sans-serif",
 						        margin: 1,
 						        maxSize: new go.Size(160, NaN),
 						        wrap: go.TextBlock.WrapFit,
 						        editable: true
 						        },new go.Binding("text","text")),
 						    $(go.TextBlock,{ 
 							  	font: "bold 11pt Helvetica, Arial, sans-serif",
 							    margin: 0,
 							    text:" [",
 							    isMultiline: false }),
 							$(go.TextBlock,{
 							    font: "bold 11pt Helvetica, Arial, sans-serif",
 								margin: 0,
 							    maxSize: new go.Size(160, NaN),
 							    wrap: go.TextBlock.WrapFit,
 							    editable: true
 							    },new go.Binding("text","first").makeTwoWay()),
 							$(go.TextBlock,{ 
 								font: "bold 11pt Helvetica, Arial, sans-serif",
 								margin: 0,
 								text:"]",
 								isMultiline: false }),
 							$(go.TextBlock,{ 
 					           	font: "bold 11pt Helvetica, Arial, sans-serif",
 					           	margin: 2,
 					        	text:"",
 					            isMultiline: false })
 			            	)
 			        	)
 		        	),
 		    // three named ports, one on each side:
 		    makePort("T", go.Spot.Top, false, true),
            makePort_while_for("Finish While","L"),
            makePort_while_for("Escape While","R"),
            makePort_while_for("True","B")
 		));
        

    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        {
          routing: go.Link.AvoidsNodes,
          curve: go.Link.JumpOver,
          corner: 5, toShortLength: 4,
          relinkableFrom: true,
          relinkableTo: true,
          reshapable: true,
          resegmentable: true,
          // mouse-overs subtly highlight links:
          mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
          mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
        },
        new go.Binding("points").makeTwoWay(),
        $(go.Shape,  // the highlight shape, normally transparent
          { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
        $(go.Shape,  // the link path shape
          { isPanelMain: true, stroke: "gray", strokeWidth: 2 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null, fill: "gray"}),
        $(go.Panel, "Auto",  // the link label, normally not visible
          { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5},
          new go.Binding("visible", "visible").makeTwoWay()
        )
      );

    // Make link labels visible if coming out of a "conditional" node.
    // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
    function showLinkLabel(e) {
      var label = e.subject.findObject("LABEL");
      if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
    }

        // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
        myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
        myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;
        load();  // load an initial diagram from some JSON text
    
        // basic
        var myPalette = $(go.Palette, "myPaletteDiv");
        myPalette.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette.model.nodeDataArray =[
        	{ category: "Start", text: "Start" },
            { category: "End", text: "End" },           
        	{ category: "Variable", text:"int",first:"name",second:"initial value" },
            { category: "Variable", text:"char",first:"name",second:"initial value" },
            { category: "Variable", text:"double",first:"name",second:"initial value" },
            { category: "Variable", text:"String",first:"name",second:"initial value" },
        	{ category: "Equal", text:"Equal",first:"first item",second:"second item", figure: "SquareArrow"},
        ];
     	// operation
        var myPalette2 = $(go.Palette, "myPaletteDiv2");
        myPalette2.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette2.model.nodeDataArray =[
        	{ category: "Addition", text:"+", first:"first item",second:"second item", third:"third item", figure: "SquareArrow"},
        	{ category: "Substraction", text:"-", first:"first item",second:"second item", third:"third item", figure: "SquareArrow"},
        	{ category: "Multiplication", text:"*", first:"first item",second:"second item", third:"third item", figure: "SquareArrow"},
        	{ category: "Division", text:"/", first:"first item",second:"second item", third:"third item", figure: "SquareArrow"},
        	{ category: "Remainder", text:"%", first:"first item",second:"second item", third:"third item", figure: "SquareArrow"}
        ];
     	// array
       	var myPalette3 = $(go.Palette, "myPaletteDiv3");
        myPalette3.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette3.model.nodeDataArray =[
            { category: "Array", text: "Array",first:"type",second:"name", figure: "rectangle" },
            { category: "Append", text: "Append", first:"target array", second:"value or variable", figure: "rectangle"},
            { category: "Insert", text: "Insert",first:"target array", second:"location", third:"value or variable", figure: "rectangle"},
            { category: "Remove", text: "Remove",first:"target array", second:"location", figure: "rectangle"}
        ];
        // loop
        var myPalette4 = $(go.Palette, "myPaletteDiv4");
        myPalette4.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette4.model.nodeDataArray =[
            { category:"If", text: "If", first:"condition", figure: "Diamond" },
            { category: "End if",  figure: "Circle" },
            { category:"For", text: "For", first:"minimum value", second:"maximum value", third:"value", figure: "Diamond" },
            { category:"While", text: "While", first:"condition", figure: "Diamond" }
        ];
    
        // function
        var myPalette5 = $(go.Palette, "myPaletteDiv5");
        myPalette5.nodeTemplateMap = myDiagram.nodeTemplateMap;
        myPalette5.model.nodeDataArray =[
            { category:"Method", text: "Func", first:"return type", second:"name", third:"parameter(only variable(s))"},
            { category:"Return", text: "Return", first:"variable name or value"},
            { category:"Print", text: "Print", first:"plaintext or variable"}
        ];
    	
        
        document.getElementById("defaultOpen").click();
    } // end init
    // Make all ports on a node visible when the mouse is over the node
    function showPorts(node, show) {
        var diagram = node.diagram;
        if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
        node.ports.each(function(port) {
        port.stroke = (show ? "white" : null);
      });
    }
    // Show the diagram's model in JSON format that the user may edit
    function save() {
        document.getElementById("mySavedModel").value = myDiagram.model.toJson();
        var data = JSON.parse(myDiagram.model.toJson());
        var if_count=0;
        var endif_count=0;
        var start_count=0;
        var end_count=0;
        for(var i = 0; i<data.nodeDataArray.length; i++){
            var current_category = data.nodeDataArray[i].category;
            if(current_category == "If"){
                if_count++;
            }
            if(current_category == "End if"){
                endif_count++;
            }
            if(current_category == "Start"){
            	start_count++;
            }
			if(current_category == "End"){
            	end_count++;
            }
        }
        if(if_count!=endif_count){
            alert("WARNING :: You must only use 'End if' element after finish making if statement. See how to use it in the documents or tutorial.")
        } else if(start_count == 0 || end_count  == 0){
        	alert("WARNING :: You must use 'Start' and 'End' element when you make a flowchart. See how to use it in the documents or tutorial.")
        }
        
        else{
        	var message ="";
        	for(var j = 0; j<data.linkDataArray.length; j++){
        		var current_category2 = data.linkDataArray[j];
        		message+=current_category2+"\n";
        	}
            alert(message);
        }
        myDiagram.isModified = false;
    }
    function load() {
        myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    }
  
    function send(){
    	var data = JSON.parse(myDiagram.model.toJson());
        var if_count=0;
        var endif_count=0;
        var start_count=0;
        var end_count=0;
        var link_count=0;
        for(var i = 0; i<data.nodeDataArray.length; i++){
            var current_category = data.nodeDataArray[i].category;
            if(current_category == "If"){
                if_count++;
            }
            if(current_category == "End if"){
                endif_count++;
            }
            if(current_category == "Start"){
            	start_count++;
            }
			if(current_category == "End"){
            	end_count++;
            }
        }
        link_count = data.linkDataArray.length;
        if(if_count!=endif_count){
            alert("WARNING :: You must only use 'End if' element after finish making if statement. See how to use it in the documents or tutorial.")
        }else if(start_count == 0 || end_count  == 0){
        	alert("WARNING :: You must use 'Start' and 'End' element when you make a flowchart. See how to use it in the documents or tutorial.")
        }else if(link_count == 0){
        	alert("WARNING :: You must connect each node when you make a flowchart. See how to use it in the documents or tutorial.")
        }else{
      		var theForm, newInput1, newInput2;
    	 	var data = myDiagram.model.toJson();
    	  	var obj = document.getElementsByName('language');
    	  	var checked_index = -1;
    	  	var checked_value = "";
    	  	for( i=0; i<obj.length; i++ ) {
    			if(obj[i].checked) {
    		  		checked_index = i;
    		  		checked_value = obj[i].value;
    		  		break;
    			}
    	  	}
    	  	if(checked_value == ""){
    	  		alert("WARNING :: You must choose one of three programming language when you want to generate the source code.")
    	  	}else{
    	  		theForm = document.createElement('form');
    	  		theForm.action = "/create";
    	  		theForm.method = "post";
    	  
 		   	  	newInput1 = document.createElement('input');
    		  	newInput1.type = 'text';
    	  		newInput1.id = 'language';
    	  		newInput1.name = 'language';
    		  	newInput1.value = checked_value;
    	  
    		  	theForm.appendChild(newInput1);
    		  
    		  	newInput2 = document.createElement('input');
    		  	newInput2.type = 'text';
    		  	newInput2.id = 'flowchart_data';
    		  	newInput2.name = 'flowchart_data';
    		  	newInput2.value = data;
    		  	
    		  	theForm.appendChild(newInput2);
    	  
    		  	document.getElementById('hidden_form_container').appendChild(theForm);
    		  	theForm.submit();		
    	  	}    		  
        }		
    }
  
    function changePalette(evt,divID) {
	    if(divID == "myPaletteDiv"){
			document.getElementById("myPaletteDiv").style.display="";
		    document.getElementById("myPaletteDiv2").style.display="none";
		    document.getElementById("myPaletteDiv3").style.display="none";
		    document.getElementById("myPaletteDiv4").style.display="none";
		    document.getElementById("myPaletteDiv5").style.display="none";
	    }else if(divID == "myPaletteDiv2"){
		    document.getElementById("myPaletteDiv").style.display="none";
		    document.getElementById("myPaletteDiv2").style.display="";
		    document.getElementById("myPaletteDiv3").style.display="none";
		    document.getElementById("myPaletteDiv4").style.display="none";
		    document.getElementById("myPaletteDiv5").style.display="none";
	    }else if(divID == "myPaletteDiv3"){
		    document.getElementById("myPaletteDiv").style.display="none";
		    document.getElementById("myPaletteDiv2").style.display="none";
		    document.getElementById("myPaletteDiv3").style.display="";
		    document.getElementById("myPaletteDiv4").style.display="none";
		    document.getElementById("myPaletteDiv5").style.display="none";
	    }else if(divID == "myPaletteDiv4"){
			document.getElementById("myPaletteDiv").style.display="none";
			document.getElementById("myPaletteDiv2").style.display="none";
			document.getElementById("myPaletteDiv3").style.display="none";
			document.getElementById("myPaletteDiv4").style.display="";
			document.getElementById("myPaletteDiv5").style.display="none";
		}else if(divID == "myPaletteDiv5"){
			document.getElementById("myPaletteDiv").style.display="none";
			document.getElementById("myPaletteDiv2").style.display="none";
			document.getElementById("myPaletteDiv3").style.display="none";
			document.getElementById("myPaletteDiv4").style.display="none";
			document.getElementById("myPaletteDiv5").style.display="";
		}
    }
  
</script>
</head>
<body onload="init()">
	<nav class="navbar navbar-inverse navbar-custom">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>                        
      			</button>
      			<a class="navbar-brand" href="/">Code Generator</a>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
        			<li><a href="/">Home</a></li>
        			<li><a href="#">About</a></li>
        			<li><a href="#">Contact</a></li>
      			</ul>
    		</div>
  		</div>
	</nav>

    <h3 style="margin-left: 15px;">Flowchart Board</h3>
    <p style="margin-left: 15px;">
    	Mouse-over a Node to view its ports.
        Drag from these ports to create new Links.
        Selecting Links allows you to re-shape and re-link them.
        Selecting a Node and then clicking its TextBlock will allow
        you to edit the content (except on the Start and End Nodes).
    </p>
    <div class="container-fluid text-center" style="padding-top:3px">
        <div id="sample">
            <div style="width: 100%; padding-left: 0px; padding-right: 0px; display: flex; justify-content: space-between">
                <div class="tab col-sm-1">
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv')" id="defaultOpen">Basic</button>
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv2')">Operation</button>
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv3')">Array</button>
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv4')">Statement</button>
                    <button class="tablinks" onclick="changePalette(event, 'myPaletteDiv5')">Function</button>
                </div>
                <div id="myPaletteDiv" class="col-sm-3" style="margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myPaletteDiv2" class="col-sm-3" style="margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myPaletteDiv3" class="col-sm-3" style="margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myPaletteDiv4" class="col-sm-3" style="margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myPaletteDiv5" class="col-sm-3" style="margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
                <div id="myDiagramDiv" class="col-sm-7" style="flex-grow: 1; height: 550px; border: solid 1px black"></div>
                <div class="col-sm-1">
                	<p>Programming<br>Language :</p>
                	<p><input type='radio' name='language' value='python'/>Python</p>
                	<p><input type='radio' name='language' value='java' />JAVA</p>
			    	<p><input type='radio' name='language' value='c' />C</p>
			    	<p><button id="SaveButton" onclick="send()">Generate</button></p>      
			    	<br>
			    	<p>If you need help:</p>
			    	<p><a href="/tutorial">Tutorial page</a></p>           	
                </div>
            </div>
            <button onclick="save()">save</button>
            <button onclick="load()">Load</button>
            <div id="hidden_form_container" style="display:none;"></div>
            <textarea id="mySavedModel" style="display:none;width:100%;height:300px">
                { "class": "go.GraphLinksModel",
  				  "linkFromPortIdProperty": "fromPort",
  				  "linkToPortIdProperty": "toPort",
  				  "nodeDataArray": [ 
				{"category":"Start", "text":"Start", "key":-3, "loc":"-92.890625 -203"},
				{"category":"End", "text":"End", "key":-4, "loc":"-90.890625 142"}
				],
  				 "linkDataArray": []}
            </textarea>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
    	<div class="modai-dialog">
    		<div class="modal-content">
				<div class="modal-header">
		    		<h3>Before to use</h3>
		     	</div>
		     	<div class="modal-body">
		     		<p>There are four fundamental rules</p>
		     		<ol>
		     			<li> You must use 'Start' and 'End' node to build the flowchart.</li>
		     			<li> Do not remove [ ] brackets when you edit inside of brackets in each node.</li>
		     			<li> You must connect the nodes in order of flowchart structure.</li>
		     			<li> To use the 'If' node, you must use the 'End if' node to complete the if statement. </li>
		     		</ol>
		     	</div>
		     	<div class="modal-footer">
		     		<button type="button" class="btn btn-default" data-dismiss="modal">I got it</button>
		     		
		     	</div>    		
    		</div>
    	</div>
    </div>
	<div id="footer" class="text-center">
  		<p>Designed by Janghoon Kang</p>
	</div>
</body>
<script type="text/javascript">
	$(window).on('load',function(){
		$('#myModal').modal('show');
	});
</script>
</html>
<!--  -->